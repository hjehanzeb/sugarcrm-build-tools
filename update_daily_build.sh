cd /var/www/html/Sugar/test/build_daily/ent/sugarcrm
cp -rf modules/. /var/www/html/Sugar/build_daily/ent/sugarcrm/modules
# Empty the 'custom' directory since it's possible that files are removed
cd /var/www/html/Sugar/build_daily/ent/sugarcrm/custom
git rm -rf *
cd /var/www/html/Sugar/test/build_daily/ent/sugarcrm

# Now copy the files to custom again
cp -rf custom/. /var/www/html/Sugar/build_daily/ent/sugarcrm/custom
cp -rf include/. /var/www/html/Sugar/build_daily/ent/sugarcrm/include
cp -rf data/. /var/www/html/Sugar/build_daily/ent/sugarcrm/data
cp -rf clients/. /var/www/html/Sugar/build_daily/ent/sugarcrm/clients
cp -rf sidecar/. /var/www/html/Sugar/build_daily/ent/sugarcrm/sidecar
cp -rf src/. /var/www/html/Sugar/build_daily/ent/sugarcrm/src
cd /var/www/html/Sugar/build_daily

echo "Committing"
git add .
git commit -m "after merge of daily build on $(date)" --no-verify
echo "Done"
