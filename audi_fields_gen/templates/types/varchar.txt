$dictionary['<module_singular>']['fields']['<field_name>'] = array(
    'name' => '<field_name>',
    'vname' => 'LBL_<field_name_uppercase>',
    'type' => '<field_type>',
    'reportable' => <is_reportable>,
    'required' => <is_required>,
    'audited' => <is_auditable>,
    'importable' => <is_importable>,
    'unified_search' => <is_searchable>,
    'help' => "<field_help>",
    'comment' => "<field_comment>",
    'default_value' => "<default_value>",
    'duplicate_merge' => 'disabled',
    'size' => '20',
    'rows' => '3',
    'cols' => '20',
    'len' => '<field_len>'
);