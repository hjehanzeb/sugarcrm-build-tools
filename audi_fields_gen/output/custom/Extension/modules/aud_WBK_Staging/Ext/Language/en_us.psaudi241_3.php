<?php

$mod_strings['LBL_AUD_HOUSE_NUMBER'] = "House Number";
$mod_strings['LBL_AUD_COUNTRY_ISO3'] = "Country ISO3 Code";
$mod_strings['LBL_AUD_POSTVORRAUSVVG'] = "Postvorrausvvg";
$mod_strings['LBL_AUD_TRANSFER_ID'] = "TransferID";
$mod_strings['LBL_AUD_TRANSFER_SEQ'] = "TransferSEQ";
$mod_strings['LBL_AUD_HDL_SEQ'] = "Dealer Sequence number";
$mod_strings['LBL_AUD_DATEI'] = "Datei";
$mod_strings['LBL_AUD_CUSTOMER_TYPE'] = "Customer Type";
$mod_strings['LBL_AUD_LEASVERTRAG_ENDE'] = "End Date of Leasing Contract";
$mod_strings['LBL_AUD_CONTACT_DEALER_NAME'] = "Contract Dealer Name";
