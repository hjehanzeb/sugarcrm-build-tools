#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *replace_string(char *orig, const char *rep, const char *with);
char *strdup(const char *src);
void explode(const char *src, const char *tokens, char ***list, size_t *len);

void main(int argc, char const *argv[])
{
    // char *string = "499293";
    // int value = atof(string);

    // printf("String: %s\n", string);
    // printf("Value: %d\n", value);

    // return;
    int arguments_length = sizeof(argv) / sizeof(argv[0]);
    int i;
    char *text;
    char *build_name;
    char **arg_exploded;
    size_t len;

    // for (i = 0; i < argc; i++)
    // {
    //     printf("argument[%d]: %s\n", i, argv[i]);
    // }
    // printf("size of argv: %d\n", sizeof(argv));
    // printf("size of argv[0]: %d\n", sizeof(argv[1]));
    // printf("length of argv: %d\n", (int) sizeof(argv));
    // printf("argc: %d\n", argc);
    // printf("argv[1]: %s\n", argv[1]);
    text = "<?xml version='1.0' encoding='UTF-8'?>\n<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>\n<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en' lang='en'>\n<head profile='http://selenium-ide.openqa.org/profiles/test-case'>\n<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />\n<link rel='selenium.base' href='http://192.168.149.150{{BUILD_PATH}}/ent/sugarcrm' />\n<title>Sugar_Install</title>\n</head>\n<body>\n<table cellpadding='1' cellspacing='1' border='1'>\n<thead>\n<tr><td rowspan='1' colspan='3'>Sugar_Install</td></tr>\n</thead><tbody>\n<tr>\n    <td>open</td>\n    <td>{{BUILD_PATH}}/ent/sugarcrm/install.php</td>\n    <td></td>\n</tr>\n<tr>\n    <td>waitForPageToLoad</td>\n    <td></td>\n    <td></td>\n</tr>\n<tr>\n    <td>clickAndWait</td>\n    <td>id=button_next2</td>\n    <td></td>\n</tr>\n<tr>\n    <td>waitForPageToLoad</td>\n    <td></td>\n    <td></td>\n</tr>\n<tr>\n    <td>clickAndWait</td>\n    <td>id=button_next2</td>\n    <td></td>\n</tr>\n<tr>\n    <td>waitForPageToLoad</td>\n    <td></td>\n    <td></td>\n</tr>\n<tr>\n    <td>click</td>\n    <td>id=button_next2</td>\n    <td></td>\n</tr>\n<tr>\n    <td>pause</td>\n    <td>1000</td>\n    <td>1000</td>\n</tr>\n<tr>\n    <td>click</td>\n    <td>id=button_next2</td>\n    <td></td>\n</tr>\n<tr>\n    <td>assertValue</td>\n    <td>id=button_next2</td>\n    <td>on</td>\n</tr>\n<tr>\n    <td>verifyValue</td>\n    <td>id=button_next2</td>\n    <td>on</td>\n</tr>\n<tr>\n    <td>clickAndWait</td>\n    <td>id=button_next</td>\n    <td></td>\n</tr>\n<tr>\n    <td>pause</td>\n    <td>4000</td>\n    <td>4000</td>\n</tr>\n<tr>\n    <td>type</td>\n    <td>name=setup_license_key</td>\n    <td>1a0848b4dba81062d28bdbb1e00fc6f4</td>\n</tr>\n<tr>\n    <td>clickAndWait</td>\n    <td>id=button_next</td>\n    <td></td>\n</tr>\n<tr>\n    <td>waitForPageToLoad</td>\n    <td></td>\n    <td></td>\n</tr>\n<tr>\n    <td>clickAndWait</td>\n    <td>id=button_next2</td>\n    <td></td>\n</tr>\n<tr>\n    <td>waitForPageToLoad</td>\n    <td></td>\n    <td></td>\n</tr>\n<tr>\n    <td>type</td>\n    <td>id=setup_db_database_name</td>\n    <td>sugarcrm_{{BUILD_NAME}}</td>\n</tr>\n<tr>\n    <td>type</td>\n    <td>id=setup_db_host_name</td>\n    <td>localhost</td>\n</tr>\n<tr>\n    <td>type</td>\n    <td>id=setup_db_admin_user_name</td>\n    <td>root</td>\n</tr>\n<tr>\n    <td>type</td>\n    <td>id=setup_db_admin_password_entry</td>\n    <td>sugarcrm</td>\n</tr>\n<tr>\n    <td>clickAndWait</td>\n    <td>id=button_next2</td>\n    <td></td>\n</tr>\n<tr>\n    <td>pause</td>\n    <td>2000</td>\n    <td>2000</td>\n</tr>\n<tr>\n    <td>type</td>\n    <td>name=setup_site_admin_password</td>\n    <td>123</td>\n</tr>\n<tr>\n    <td>type</td>\n    <td>name=setup_site_admin_password_retype</td>\n    <td>123</td>\n</tr>\n<tr>\n    <td>clickAndWait</td>\n    <td>id=button_next2</td>\n    <td></td>\n</tr>\n<tr>\n    <td>pause</td>\n    <td>2000</td>\n    <td>2000</td>\n</tr>\n<tr>\n    <td>clickAndWait</td>\n    <td>id=button_next2</td>\n    <td></td>\n</tr>\n<tr>\n    <td>pause</td>\n    <td>2000</td>\n    <td>2000</td>\n</tr>\n<tr>\n    <td>clickAndWait</td>\n    <td>id=button_next2</td>\n    <td></td>\n</tr>\n<tr>\n    <td>pause</td>\n    <td>120000</td>\n    <td>120000</td>\n</tr>\n<tr>\n    <td>clickAndWait</td>\n    <td>id=button_next2</td>\n    <td></td>\n</tr>\n<tr>\n    <td>clickAndWait</td>\n    <td>id=button_next2</td>\n    <td></td>\n</tr>\n</tbody></table>\n</body>\n</html>";
    
    // Getting build name from build path
    explode(argv[2], "/", &arg_exploded, &len);
    build_name = arg_exploded[len - 1];
    
    // replacing the template variables
    text = replace_string(text, "{{BUILD_PATH}}", argv[2]);
    text = replace_string(text, "{{BUILD_NAME}}", build_name);

    // printf("%s\n", text);
    FILE *fh;
    fh = fopen("Sugar_Install", "w");
    fputs(text, fh);
    fclose(fh);

    // system("sudo service httpd restart");
}

char *replace_string(char *orig, const char *rep, const char *with) {
    char *result; // the return string
    char *ins;    // the next insert point
    char *tmp;    // varies
    int len_rep;  // length of rep (the string to remove)
    int len_with; // length of with (the string to replace rep with)
    int len_front; // distance between rep and end of last rep
    int count;    // number of replacements

    // sanity checks and initialization
    if (!orig || !rep)
        return NULL;
    len_rep = strlen(rep);
    if (len_rep == 0)
        return NULL; // empty rep causes infinite loop during count
    if (!with)
        with = "";
    len_with = strlen(with);

    // count the number of replacements needed
    ins = orig;
    for (count = 0; tmp = strstr(ins, rep); ++count) {
        ins = tmp + len_rep;
    }

    tmp = result = malloc(strlen(orig) + (len_with - len_rep) * count + 1);

    if (!result)
        return NULL;

    // first time through the loop, all the variable are set correctly
    // from here on,
    //    tmp points to the end of the result string
    //    ins points to the next occurrence of rep in orig
    //    orig points to the remainder of orig after "end of rep"
    while (count--) {
        ins = strstr(orig, rep);
        len_front = ins - orig;
        tmp = strncpy(tmp, orig, len_front) + len_front;
        tmp = strcpy(tmp, with) + len_with;
        orig += len_front + len_rep; // move to next "end of rep"
    }
    strcpy(tmp, orig);
    return result;
}

char *strdup(const char *src)
{
    char *tmp = malloc(strlen(src) + 1);
    if(tmp)
        strcpy(tmp, src);
    return tmp;
}

void explode(const char *src, const char *tokens, char ***list, size_t *len)
{   
    if(src == NULL || list == NULL || len == NULL)
        return;

    char *str, *copy, **_list = NULL, **tmp;
    *list = NULL;
    *len  = 0;

    copy = strdup(src);
    if(copy == NULL)
        return;

    str = strtok(copy, tokens);
    if(str == NULL)
        goto free_and_exit;

    _list = realloc(NULL, sizeof *_list);
    if(_list == NULL)
        goto free_and_exit;

    _list[*len] = strdup(str);
    if(_list[*len] == NULL)
        goto free_and_exit;
    (*len)++;


    while((str = strtok(NULL, tokens)))
    {   
        tmp = realloc(_list, (sizeof *_list) * (*len + 1));
        if(tmp == NULL)
            goto free_and_exit;

        _list = tmp;

        _list[*len] = strdup(str);
        if(_list[*len] == NULL)
            goto free_and_exit;
        (*len)++;
    }


free_and_exit:
    *list = _list;
    free(copy);
}