<?php

/**
 * This file accepts arguments in the format as follows:
 * --bnr        Dealer's BNR in the format --bnr=12345
 * --count      Number of Contacts to be generated
 * --dealer-id  The UID of the Dealer
 *
 * Example: php cgen.php --bnr=DE123 --count=10 --dealer-id=<Dealer ID> | mysql -u<MySQL username> -p<MySQL password> -C <database name>
 */

define('sugarEntry', true);
define('ENTRY_POINT_TYPE', 'api');
ini_set('display_errors', 'Off');
set_time_limit(0);

require_once ('maintainance_data.php');

$arg_count = '100';
$arg_dealer_id = '3650247c-3307-11e6-9fa4-080027b877ea';
$arg_bnr = 'DE123';

// Get arguments
for ($i=1; $i < count($argv); $i++) {
    $argument = $argv[$i];
    $exploded_arg = explode('=', str_replace('--', '', $argument));

    $variable_name = 'arg_' . str_replace('-', '_', strtolower($exploded_arg[0]));
    $$variable_name = $exploded_arg[1];
}
// echo "Testing variables:\nCount = $arg_count\nDealer ID: $arg_dealer_id\nBNR: $arg_bnr\n";

// require_once 'include/entryPoint.php';
// $GLOBALS['current_user'] = BeanFactory::getBean('Users')->getSystemUser();

//==========================================================================================================
// For Contacts
//==========================================================================================================
$insert_columns = array(
    'id' => "'{{ID}}'",
    'date_entered' => "now()",
    'date_modified' => "now()",
    'modified_user_id' => "'1'",
    'created_by' => "'1'",
    'description' => "NULL",
    'deleted' => "0",
    'salutation' => "'{{SALUTATION}}'",
    'first_name' => "'{{FIRST_NAME}}'",
    'last_name' => "'{{LAST_NAME}}'",
    'title' => "NULL",
    'facebook' => "NULL",
    'twitter' => "NULL",
    'googleplus' => "NULL",
    'department' => "NULL",
    'do_not_call' => "0",
    'phone_home' => "'{{PHONE_HOME}}'",
    'phone_mobile' => "'{{PHONE_MOBILE}}'",
    'phone_work' => "'{{PHONE_WORK}}'",
    'phone_other' => "NULL",
    'phone_fax' => "'{{PHONE_FAX}}'",
    'primary_address_street' => "'{{PRIMARY_ADDRESS_STREET}}'",
    'primary_address_city' => "'{{PRIMARY_ADDRESS_CITY}}'",
    'primary_address_state' => "NULL",
    'primary_address_postalcode' => "'{{PRIMARY_ADDRESS_POSTALCODE}}'",
    'primary_address_country' => "'DEU'",
    'alt_address_street' => "NULL",
    'alt_address_city' => "NULL",
    'alt_address_state' => "NULL",
    'alt_address_postalcode' => "NULL",
    'alt_address_country' => "NULL",
    'assistant' => "NULL",
    'assistant_phone' => "NULL",
    'picture' => "NULL",
    'lead_source' => "NULL",
    'dnb_principal_id' => "NULL",
    'reports_to_id' => "NULL",
    'birthdate' => "NULL",
    'portal_name' => "NULL",
    'portal_active' => "0",
    'portal_password' => "NULL",
    'portal_app' => "NULL",
    'preferred_language' => "'de_DE'",
    'campaign_id' => "''",
    'mkto_sync' => "0",
    'mkto_id' => "NULL",
    'mkto_lead_score' => "NULL",
    'assigned_user_id' => "''",
    'team_id' => "'1'",
    'team_set_id' => "'1'",
    'aud_adel_dd' => "'Freiherr'",
    'aud_primary_address' => "NULL",
    'aud_service_user_id' => "''",
    'aud_set_phrase' => "'Sehr geehrter {{SALUTATION_TRANSLATED}} {{LAST_NAME}}'",
    'aud_interface_dsmaviav_accepted_flag' => "1",
    'aud_phone_mobile_private' => "NULL",
    'aud_hdl_number' => "NULL",
    'aud_email_de_changed_by_id' => "'1'",
    'aud_eva_error_message' => "NULL",
    'aud_has_prim_contact_flag' => "0",
    'aud_middle_name' => "'{{FIRST_NAME}}'",
    'aud_new_car_user_id' => "''",
    'aud_data_update' => "NULL",
    'aud_phone_updated' => "NULL",
    'aud_phone_de_change_date' => "NULL",
    'aud_eva_lead_id' => "NULL",
    'aud_counter_registration_balance_reset' => "NULL",
    'aud_email_updated' => "NULL",
    'aud_dealer_id' => "'{{AUD_DEALER_ID}}'",
    'aud_email_mailing_change_date' => "NULL",
    'aud_de_to_be_circulated_flag' => "0",
    'aud_assistance_status_dd' => "'Standard'",
    'aud_dealer_bnr' => "'{{AUD_DEALER_BNR}}'",
    'aud_address_updated' => "NULL",
    'aud_registration_balance_year_counter' => "NULL",
    'aud_address_de_flag' => "1",
    'aud_phone_updated_by_id' => "'1'",
    'aud_address_updated_by_id' => "'1'",
    'aud_interface_ttcall_accepted_flag' => "1",
    'aud_homepage' => "NULL",
    'aud_update_by_interface' => "NULL",
    'aud_category_of_interest' => "NULL",
    'aud_interface_newada_accepted_flag' => "1",
    'aud_po_box' => "'{{AUD_PO_BOX}}'",
    'aud_birthday_day' => "{{AUD_BIRTHDAY_DAY}}",
    'aud_contact_blocked_flag' => "0",
    'aud_birth_year' => "{{AUD_BIRTH_YEAR}}",
    'aud_contact_convert_to_company_contactID' => "''",
    'aud_dms_contact_number' => "NULL",
    'aud_kommission_nr' => "NULL",
    'aud_reason_for_blocking_date' => "NULL",
    'aud_external_id' => "NULL",
    'aud_email_mailing_de_flag' => "1",
    'aud_contact_convert_to_company_flag' => "0",
    'aud_company_1' => "NULL",
    'aud_customer_data_disclosure' => "'N'",
    'aud_job_title' => "NULL",
    'aud_pending_updates_flag' => "0",
    'aud_date_update' => "NULL",
    'aud_interface_agis_accepted_flag' => "1",
    'aud_reason_for_blocking' => "NULL",
    'aud_dmariav_primary_customer_group' => "NULL",
    'aud_has_send_to_eva_flag' => "0",
    'aud_address_de_change_date' => "NULL",
    'aud_reason_for_blocking_source' => "NULL",
    'aud_phone_de_changed_by_id' => "'1'",
    'aud_industry_dd' => "NULL",
    'aud_email_de_change_date' => "NULL",
    'aud_title_dd' => "'{{AUD_TITLE_DD}}'",
    'aud_company_2' => "NULL",
    'aud_address_de_changed_by_id' => "'1'",
    'aud_used_car_user_id' => "''",
    'aud_bonosrelevant_flag' => "0",
    'aud_phone_fax_private' => "'{{AUD_PHONE_FAX_PRIVATE}}'",
    'aud_kommission_year' => "NULL",
    'aud_reason_for_unblocking_date' => "NULL",
    'aud_membership_number' => "NULL",
    'aud_icon_type' => "NULL",
    'aud_email_de_flag' => "1",
    'aud_interface_eva_accepted_flag' => "1",
    'aud_phone_de_flag' => "1",
    'aud_newada_customer_number' => "'{{AUD_NEWADA_CUSTOMER_NUMBER}}'",
    'aud_registration_balance_counter' => "NULL",
    'aud_sub_contact_types_dd' => "NULL",
    'aud_is_fsag_contact_flag' => "0",
    'aud_ISO_3_country' => "NULL",
    'aud_fsag_consultant_id' => "NULL",
    'aud_fsag_academic_title' => "NULL",
    'aud_fsag_address_house_no' => "NULL",
    'aud_fsag_postvorrausvvg' => "NULL",
    //'aud_fsag_transferseq' => "NULL",
    //'aud_fsag_hdlseq' => "NULL",
    'aud_fsag_datei' => "NULL",
    'aud_fsag_kundenart' => "NULL",
    'aud_fsag_leasvertragende' => "NULL",
    'aud_fsag_kontakt_hdlname' => "NULL",
    'aud_fsag_is_leasing_mailing' => "0",
    'aud_de_resolution_type_dd' => "NULL",
    'aud_icon_de' => "NULL",
    'aud_birthday_month' => "0",
    'aud_membership_number_valid_from' => "NULL",
    'aud_reason_for_blocking_note' => "NULL",
    'aud_email_updated_by_id' => "''",
    'aud_membership_number_valid_until' => "NULL",
    'aud_dmariav_skr51_customer_group' => "NULL",
    'aud_email_mailing_de_changed_by' => "''",
    'aud_name_suffix' => "''",
    'aud_job_position' => "NULL",
    'aud_job' => "NULL",
    'aud_contact_type' => "'Person'",
    'aud_recycel_bin_flag' => "0",
    'aud_ldb_id' => "NULL",
    // 'aud_work_address_street' => "NULL",
    // 'aud_work_address_city' => "NULL",
    // 'aud_work_address_state' => "NULL",
    // 'aud_work_address_postalcode' => "NULL",
    // 'aud_work_address_country' => "NULL",
    'aud_kaeuferart' => "NULL",
    'aud_source' => "'sugar'",
    'aud_sonderabnehmergruppe' => "NULL",
    'aud_juristische_person' => "0",
    'aud_is_duplicate_flag' => "0",
    'aud_fsag_is_finance_mailing' => "0",
    'aud_dms_contact_number_old' => "NULL",
    'aud_dms_kennzeichen' => "NULL",
    'aud_is_aplus_member_flag' => "0",
    'aud_lead_flag' => "0",
    'aud_employment_title' => "NULL",
    'aud_contact_type_dd' => "'0'"
);

$sql = "INSERT INTO `contacts` (".implode(', ', array_keys($insert_columns)).") VALUES ";
$sql_template = "(".implode(', ', array_values($insert_columns)).")";

//-----------------------------------------------------------------------------------------------

$sql_email_addr = <<<SQL_EMAIL_ADDR
INSERT into email_addresses (id, email_address, email_address_caps, invalid_email, opt_out, date_created, date_modified, deleted, aud_bounce_state) VALUES 
SQL_EMAIL_ADDR;

$sql_email_addr_template = <<<SQL_EMAIL_ADDR_TEMPLATE
('{{EMAIL_ADDR_ID}}','{{FIRST_NAME}}.{{LAST_NAME}}@mailinator.com',UPPER('{{FIRST_NAME}}.{{LAST_NAME}}@mailinator.com'),
  '0','0',NOW(),NOW(),0,'')
SQL_EMAIL_ADDR_TEMPLATE;

//-----------------------------------------------------------------------------------------------

$sql_email_bean_rel = <<<SQL_EMAIL_BEAN_REL
INSERT INTO email_addr_bean_rel (id, email_address_id, bean_id, bean_module, primary_address, reply_to_address, date_created, date_modified, deleted) VALUES 
SQL_EMAIL_BEAN_REL;

$sql_email_bean_rel_template = <<<SQL_EMAIL_BEAN_REL_TEMPLATE
(UUID(),'{{EMAIL_ADDR_ID}}','{{ID}}','Contacts','1','0',NOW(),NOW(),0)
SQL_EMAIL_BEAN_REL_TEMPLATE;

//-----------------------------------------------------------------------------------------------

$salutations = ['Mr.','Mrs.'/*,'Mr_Mrs.'*/];
$fnames = ['Martina','Marina','Michael','Simone','Franziska','Kristian','Mike','Marco','Birgit','Kevin','Lena','Leonie','Silke','Marcel','Petra','Gabriele','Annett','Sophia','Jörg','Barbara','Dennis','Jana','Heike','Lea','Karolin','Vanessa','Luca','Janina','Lukas','Markus','Alexander','Maximilian','Kathrin','Ralph','Anne','Nadine','Mathias','Benjamin','Susanne','Manuela','Katharina','Wolfgang','Julia','Marie','Mandy','Paul','Dieter','Diana','Patrick','Laura','Sara','Monika','Jennifer','Uwe','Sven','Robert','Dominik','Steffen','Claudia','Marko','Nicole','Christine','Jan','Maria','Sandra','Philipp','Daniel','Stefanie','Yvonne','Stefan','René','Katrin','Martin','Christina','Matthias','Mario','Anna','Max','Tim','Phillipp','Stephan','David','Niklas','Thorsten','Juliane','Maik','Erik','Torsten','Sebastian','Daniela','Kerstin','Thomas','Klaus','Angelika','Anja','Swen','Sarah','Michelle','Jens','Klaudia','Ulrike','Jonas','Karin','Brigitte','Leon','Andrea','Bernd','Eric','Jürgen','Dirk','Lucas','Ulrich','Jessica','Christin','Florian','Ursula','Andreas','Antje','Sabine','Melanie','Felix','Christian','Ines','Peter','Jessika','Tobias','Leah','Uta','Stephanie','Doreen','Ralf','Anke','Johanna','Sophie','Sabrina','Katja','Ute','Frank','Lisa','Tanja','Tom','Kristin'];
$lnames = ['Schultheiss','Gruenewald','Schiffer','Hertzog','Theiss','Wirtz','Fuerst','Furst','Naumann','Gerste','Lemann','Vogler','Kaufmann','Wolf','Roth','Bauer','Decker','Beyer','Beich','Baum','Waechter','Zweig','Schneider','Wulf','Metzger','Krüger','Reinhardt','König','Amsel','Bach','Achen','Muench','Duerr','Lang','Himmel','Fuhrmann','Braun','Hirsch','Goldschmidt','Boehm','Maier','Farber','Schuster','Fassbinder','Schweitzer','Hueber','Gottschalk','Barth','Ritter','Werfel','Kaiser','Fink','Winkel','Drescher','Pfeifer','Herz','Jaeger','Schultz','Wurfel','Fiedler','Burger','Mayer','Moeller','Fischer','Ziegler','Friedmann','Brauer','Schroder','Werner','Sankt','Wechsler','Schwarz','Eichmann','Kunze','Pabst','Egger','Traugott','Fleischer','Nagel','Pfaff','Maur','Fisher','Finkel','Schweizer','Lehmann','Faust','Schwab','Aachen','Konig','Zimmer','Schmidt','Foerster','Bergmann','Fenstermacher','Ebersbacher','Neumann','Beike','Gerber','Junker','Baumgaertner','Beckenbauer','Faerber','Walter','Nacht','Sommer','Müller','Schröder','Lowe','Bader','Wagner','Lange','Wannemaker','Lehrer','Blau','Theissen','Urner','Meier','Jager','Gersten','Nussbaum','Loewe','Ackermann','Grunewald','Hoover','Ackerman','Krause','Kirsch','Feierabend','Freytag','Diederich','Grunwald','Kohler','Hartmann','Vogel','Klein','Bayer','Faber','Jung','Vogt','Hoch','Koehler','Cole','Neustadt','Herzog','Gaertner','Friedman','Nadel','Krueger','Bachmeier','Wirth','Hoffmann','Weiß','Schulze','Hahn','Frei','Gärtner','Koertig','Weisz','Kuhn','Baer','Neudorf','Koch','Wexler','Freeh','Pfeffer','Kuster','Freud','Baecker','Shuster','Dresner','Mueller','Zimmerman','Gottlieb','Schroeder','Thalberg','Abt','Freitag','Kastner','Frankfurter','Trommler','Weissmuller','Fruehauf','Abend','Schwartz','Reiniger','Kluge','Biermann','Berg','Osterhagen','Schmid','Kortig','Propst','Bürger','Bumgarner','Abendroth','Engel','Papst','Seiler','Drechsler','Huber','Fuchs','Eichelberger','Mehler','Eichel','Gloeckner','Fried','Sanger','Koenig','Eberhart','Busch','Schreiner','Rothstein','Kalb','Kohl','Brandt','Muller','Herrmann','Dreher','Eiffel','Unger','Bar','Zimmermann','Holzman','Eisenhauer','Hertz','Klug','Maurer','Ostermann','Strauss','Adler','Probst','Eisenhower','Frey','Schulz','Eggers','Eisenberg','Schmitt','Schmitz','Daecher','Dresdner','Dietrich','Weiss','Keller','Kuester','Luft','Frueh','Kuefer','Weber','Durr','Pfeiffer','Baier','Saenger','Meyer','Ebersbach','Peters','Reinhard','Mahler','Becker','Moench','Bohm','Austerlitz','Ehrlichmann','Möller','Richter','Kappel','Scherer','Schaefer','Köhler','Eberhardt','Bieber','Rothschild','Oster','Mauer','Scholz','Bosch','Baumgartner','Schäfer','Baader','Meister','Schreiber','Herman','Kaestner','Schuhmacher'];
$titles = ['Dr.','Dr. Prof.', ''];
$salutations_ger = [
    'Mr.' => 'Herr',
    'Mrs.' => 'Frau'
];
$street_addresses = ['Albrechtstrasse 95','Ziegelstr. 49','Eschenweg 71','Gotzkowskystrasse 67','Marseiller Strasse 98','Kastanienallee 52','Jenaer Strasse 86','Charlottenstrasse 30','Luebecker Strasse 60','Albrechtstrasse 56','Alter Wall 12','Kurfürstendamm 24','Leopoldstraße 52','Lietzenburger Strasse 34','Langenhorner Chaussee 66','Flotowstr. 1','Fugger Strasse 18','Bissingzeile 68','Fontenay 23','Fontenay 50','Motzstr. 54','Schmarjestrasse 80','Hardenbergstraße 27','Rudower Chaussee 38','Genslerstraße 57','Knesebeckstraße 53','Hardenbergstraße 50','Neuer Jungfernstieg 8','Brandenburgische Str. 21','Schoenebergerstrasse 51','Reeperbahn 46','Potsdamer Platz 16','Charlottenstrasse 91','Hermannstrasse 40','Unter den Linden 97','Ansbacher Strasse 61','Knesebeckstrasse 33','Esplanade 55','Rosenstrasse 38','Leipziger Strasse 48','Billwerder Neuer Deich 20','Storkower Strasse 26','Landhausstraße 23','Guentzelstrasse 70','Lietzenburger Strasse 37','Kantstraße 64','Boxhagener Str. 74','Buelowstrasse 7','Luckenwalder Strasse 26','Invalidenstrasse 42','Schönwalder Allee 94','Gotzkowskystraße 32','Gotzkowskystrasse 10','Guentzelstrasse 21','Büsingstrasse 29','Motzstr. 75','Waßmannsdorfer Chaussee 90','Flotowstr. 89','Pohlstrasse 88','Billwerder Neuer Deich 68','Lietzenburger Straße 92','Rohrdamm 12','Holstenwall 35','Kastanienallee 93','Kurfürstenstraße 50','Lange Strasse 26','Eschenweg 84','Mellingburgredder 68','Hardenbergstraße 85','Oldesloer Strasse 64','Lietzenburger Strasse 79','Fugger Strasse 58','Karl-Liebknecht-Strasse 22','Hoheluftchaussee 51','Jenaer Strasse 82','Bissingzeile 65','Schaarsteinweg 32','Lietzensee-Ufer 9','Lietzenburger Straße 23','Bayreuther Strasse 88','Eschenweg 34','Spresstrasse 15','Alt Reinickendorf 60','Stresemannstr. 47','Brandenburgische Str. 94','Guentzelstrasse 74','Frankfurter Allee 67','Schönwalder Allee 86','Lange Strasse 95','Potsdamer Platz 83','Motzstr. 46','Rathausstrasse 42','Fontenay 23','Potsdamer Platz 4','Meininger Strasse 89','Alsterkrugchaussee 82','Neuer Jungfernstieg 63','Grosse Praesidenten Str. 47','Alt Reinickendorf 70','Fischerinsel 88','Rankestraße 25','Leopoldstraße 7','Brandenburgische Str 42','Kirchenallee 75','Mellingburgredder 27','Alter Wall 94','Reeperbahn 27','Schoenebergerstrasse 69','Fasanenstrasse 68','Koepenicker Str. 16','Kantstrasse 57','Landhausstraße 14','Rudower Chaussee 98','Heinrich Heine Platz 24','Inge Beisheim Platz 21','Waßmannsdorfer Chaussee 31','Alt Reinickendorf 71','Reeperbahn 6','Bleibtreustraße 68','Gotzkowskystraße 53','Kurfürstendamm 58','Alsterkrugchaussee 3','Leipziger Strasse 20','Rohrdamm 45','Güntzelstrasse 26','Luebecker Strasse 54','Genterstrasse 69','Gotzkowskystrasse 73','Grolmanstraße 15','Ziegelstr. 52','Kantstrasse 83','Augsburger Straße 60','Nuernbergerstrasse 43','Leipziger Strasse 45','Bissingzeile 10','Hans-Grade-Allee 42','Brandenburgische Straße 50','Lange Strasse 6','Kantstrasse 15','Gubener Str. 88','Landhausstraße 35','Pohlstrasse 54','Karl-Liebknecht-Strasse 61','Holstenwall 93','Kieler Strasse 85','Augsburger Strasse 89','Ufnau Strasse 29','Konstanzer Strasse 65','Los-Angeles-Platz 40','Landhausstraße 36','Hedemannstasse 57','Gubener Str. 88','Kurfuerstendamm 69','Eschenweg 87','Gotzkowskystraße 81','Genslerstraße 21','Flotowstr. 65','Luetzowplatz 98','Leopoldstraße 90','Mohrenstrasse 44','Jenaer Strasse 18','Budapester Strasse 91','Lützowplatz 40','Alt-Moabit 70','Kantstrasse 93','Kantstraße 84','Leopoldstraße 26','Brandenburgische Strasse 95','Stuttgarter Platz 40','Karl-Liebknecht-Strasse 45','Meininger Strasse 45','Kantstrasse 13','Leipziger Straße 26','Fasanenstrasse 29','Genterstrasse 48','Kirchenallee 61','Borstelmannsweg 12','Konstanzer Strasse 39','Neue Roßstr. 84','Pasewalker Straße 33','Pasewalker Straße 3','Kirchenallee 88','Kurfürstendamm 66','Bleibtreustraße 92','Fischerinsel 39','Rudower Chaussee 87','Hallesches Ufer 89','Potsdamer Platz 5','Pohlstrasse 73','Koepenicker Str. 94','Flotowstr. 44','Rudolstaedter Strasse 56','Brandenburgische Str. 34','Borstelmannsweg 7','Mollstrasse 47','Luckenwalder Strasse 83','Friedrichstrasse 51','Brandenburgische Straße 47','Waldowstr. 37','Oldesloer Strasse 31','Michaelkirchstr. 76','Leopoldstraße 2','Kantstrasse 17','Brandenburgische Strasse 90','Paul-Nevermann-Platz 59','Grolmanstraße 97','Hermannstrasse 86','Rudower Chaussee 38','Augsburger Strasse 13','Guentzelstrasse 91','Kastanienallee 24','Luckenwalder Strasse 40','Stresemannstr. 28','An der Alster 30','Rudower Chaussee 72','Bleibtreustraße 66','Schaarsteinweg 78','Landhausstraße 73','Schillerstrasse 41','Hoheluftchaussee 92','Kurfuerstendamm 49','Gubener Str. 12','Hollander Strasse 23','Gubener Str. 28','Bissingzeile 64','Hallesches Ufer 71','Mühlenstrasse 13','Landhausstraße 66','Bleibtreustraße 61','Augsburger Strasse 24','Anhalter Strasse 59','Ziegelstr. 37','An der Alster 34','Rudower Strasse 46','Marseiller Strasse 35','Koenigstrasse 95','Mühlenstrasse 53','Pappelallee 76','Ollenhauer Str. 8','Kantstrasse 52','Bleibtreustraße 76','Leipziger Straße 11','Schaarsteinweg 27','Fischerinsel 26','Leipziger Straße 81','Luetzowplatz 52','Scharnweberstrasse 33','Rudolstaedter Strasse 2','Alt Reinickendorf 33','Friedrichstrasse 54','Pasewalker Straße 96','Gotzkowskystrasse 99','Unter den Linden 79','Fontenay 31','Paul-Nevermann-Platz 84','Anhalter Strasse 27','Guentzelstrasse 58','Bayreuther Strasse 50','Schillerstrasse 52','Brandenburgische Str. 91','Büsingstrasse 43','Lietzenburger Straße 40','Flughafenstrasse 10','Kirchenallee 92','Schoenebergerstrasse 49','Fasanenstrasse 86','Michaelkirchstr. 39','Brandenburgische Strasse 29','Gotzkowskystrasse 3','Rudower Chaussee 41','Frankfurter Allee 53','Rathausstrasse 91','Brandenburgische Straße 40','Koepenicker Str. 28','Storkower Strasse 76','Boxhagener Str. 53','Joachimstaler Str. 52','Büsingstrasse 77','Bleibtreustraße 49','Eichendorffstr. 72','An der Alster 19','Rudower Chaussee 64','Los-Angeles-Platz 96','Prager Str 1','Rudolstaedter Strasse 65','Meinekestraße 35','Hans-Grade-Allee 75','Fasanenstrasse 9','Charlottenstrasse 43','Karl-Liebknecht-Strasse 92','Bissingzeile 14','Lützowplatz 75','Augsburger Straße 33','Fasanenstrasse 63','Kieler Strasse 92','Rathausstrasse 75','Unter den Linden 13','Jahnstrasse 82','Alsterkrugchaussee 25','Leipziger Straße 13','Nuernbergerstrasse 9','Schönhauser Allee 20','Landsberger Allee 16','Alter Wall 77','Karl-Liebknecht-Strasse 67','Ruschestrasse 75','Unter den Linden 48','Sömmeringstr. 10','Ellmenreichstrasse 76','Kastanienallee 85','Ellmenreichstrasse 71','Ellmenreichstrasse 66','Grolmanstraße 92','Kastanienallee 54','Mohrenstrasse 37','Paul-Nevermann-Platz 70','Rankestraße 97','Alt-Moabit 93','Leipziger Straße 87','Stresemannstr. 94','Schönwalder Allee 51','Anhalter Strasse 13','Rathausstrasse 26','Unter den Linden 37','An der Alster 69','Mohrenstrasse 41','Leipziger Straße 99','Pappelallee 83','Brandenburgische Str. 16','Pasewalker Straße 97','Joachimstaler Str. 38','Schoenebergerstrasse 63','Messedamm 3','Prenzlauer Allee 97','Hallesches Ufer 28','Messedamm 39','Ansbacher Strasse 26','Hans-Grade-Allee 94','Los-Angeles-Platz 11','Potsdamer Platz 87','Buelowstrasse 30','Sömmeringstr. 74','Borstelmannsweg 23','Frankfurter Allee 22','Michaelkirchstr. 34','Leipziger Straße 54','Bissingzeile 96','Heinrich Heine Platz 39','Hollander Strasse 71','Messedamm 10','Budapester Straße 71','Rathausstrasse 69','An Der Urania 76','Pappelallee 94','Pohlstrasse 79','Prenzlauer Allee 48','Alsterkrugchaussee 71','Luetzowplatz 90','Brandenburgische Str. 32','Langenhorner Chaussee 24','Lietzenburger Strasse 69','Wallstrasse 1','Rudolstaedter Strasse 85','Ansbacher Strasse 29','Jahnstrasse 72','Hallesches Ufer 51','Hallesches Ufer 17','Hermannstrasse 67','Kurfürstenstraße 13','Mohrenstrasse 13','Gotzkowskystraße 10','Neue Roßstr. 50','Invalidenstrasse 56','Joachimstaler Str. 46','Hardenbergstraße 60','Hedemannstasse 72','Marseiller Strasse 6','Neuer Jungfernstieg 73','Konstanzer Strasse 21','Messedamm 50','Storkower Strasse 36','Brandenburgische Str. 64','Prenzlauer Allee 22','Schönhauser Allee 87','Leipziger Strasse 56','Luckenwalder Strasse 84','Neue Roßstr. 59','Pappelallee 12','Alsterkrugchaussee 74','Invalidenstrasse 85','Knesebeckstrasse 46','Schoenebergerstrasse 23','Luckenwalder Strasse 80','Guentzelstrasse 4','Prager Str 46','Marseiller Strasse 71','Alt Reinickendorf 75','Paderborner Strasse 63','Hallesches Ufer 94','Prenzlauer Allee 7','Langenhorner Chaussee 18','Ziegelstr. 49','Koepenicker Str. 93','Luebecker Strasse 68','Jenaer Strasse 81','Budapester Strasse 43','An Der Urania 26','Los-Angeles-Platz 4','Flughafenstrasse 42','Mollstrasse 66','Hochstrasse 27','Koepenicker Str. 19','Luebecker Strasse 94','An der Alster 5','Ellmenreichstrasse 40','Gotzkowskystraße 18','Brandenburgische Straße 49','Boxhagener Str. 11','Spresstrasse 37','Waldowstr. 8','Kurfürstenstraße 57','Oldesloer Strasse 16','Nuernbergerstrasse 64','Grosse Praesidenten Str. 80','Waßmannsdorfer Chaussee 1','Los-Angeles-Platz 90','Ufnau Strasse 6','Messedamm 81','Kastanienallee 82','Potsdamer Platz 50','Leopoldstraße 65','Augsburger Strasse 35','Kurfürstendamm 12','Lietzensee-Ufer 63','An Der Urania 19','Alt-Moabit 55','Straße der Pariser Kommune 37','Alter Wall 88','Knesebeckstraße 43','Landhausstraße 68','Kirchenallee 62','Brandenburgische Strasse 43','Landsberger Allee 40','Landhausstraße 91','Grolmanstraße 24','Hans-Grade-Allee 58','Marseiller Strasse 69','Gotzkowskystrasse 76','Grosse Praesidenten Str. 31','Alter Wall 5','Gotzkowskystrasse 88','Straße der Pariser Kommune 72','Motzstr. 31','Oldesloer Strasse 42','Bleibtreustrasse 44','Rhinstrasse 81','Bissingzeile 76','Brandenburgische Strasse 59','Rankestraße 60','Anhalter Strasse 48','Ansbacher Strasse 5','Brandenburgische Str. 66','Genslerstraße 39','Ruschestrasse 55','Lietzensee-Ufer 43','Bleibtreustraße 52','Am Borsigturm 91','Brandenburgische Straße 35','Ruschestrasse 53','Anhalter Strasse 31','Konstanzer Strasse 96','Gotthardstrasse 79','Schillerstrasse 16','Sömmeringstr. 83','Ansbacher Strasse 65','Koenigstrasse 83','Schoenebergerstrasse 76','Leopoldstraße 77','Lietzensee-Ufer 61','Sömmeringstr. 75','Kieler Strasse 18','Nuernbergerstrasse 72','Prenzlauer Allee 84','Mühlenstrasse 30','Flughafenstrasse 98','Alt-Moabit 9','Koepenicker Str. 98','Prenzlauer Allee 70','Luebecker Strasse 48','Nuernbergerstrasse 74','Luckenwalder Strasse 35','Büsingstrasse 68','Genslerstraße 12','Eichendorffstr. 30','Schönwalder Allee 45','Genslerstraße 89','Heinrich Heine Platz 28','Flotowstr. 54','Lietzenburger Straße 96','Borstelmannsweg 99','Alter Wall 71','Ansbacher Strasse 27','Genterstrasse 30','Spresstrasse 33','Fasanenstrasse 62','Schönhauser Allee 89','Gotzkowskystrasse 68','Bleibtreustraße 11','Hans-Grade-Allee 28','Leipziger Straße 48','Schillerstrasse 26','Frankfurter Allee 76','Sömmeringstr. 4','Mollstrasse 75','Ansbacher Strasse 56','Kantstraße 47','Hallesches Ufer 38','Inge Beisheim Platz 42','Schmarjestrasse 65','Hardenbergstraße 30','Kantstrasse 30','Prenzlauer Allee 47','Luetzowplatz 80','Hermannstrasse 1','Augsburger Straße 75','An Der Urania 19','Knesebeckstraße 8','Grosse Praesidenten Str. 21','Kantstraße 6','Lietzensee-Ufer 47','Flotowstr. 73','Schönwalder Allee 21','Koenigstrasse 90','Nuernbergerstrasse 65','Ollenhauer Str. 16','Alsterkrugchaussee 21','Esplanade 36','Oldesloer Strasse 13','Lange Strasse 77','Büsingstrasse 71','Stuttgarter Platz 33','Rhinstrasse 98','Flughafenstrasse 60','Paul-Nevermann-Platz 53','Esplanade 98','Boxhagener Str. 45','Rhinstrasse 30','Rankestraße 52','Alt-Moabit 64','Neuer Jungfernstieg 23','Gotzkowskystraße 98','Boxhagener Str. 75','Lange Strasse 14','Paul-Nevermann-Platz 35','Nuernbergerstrasse 88','Neue Roßstr. 91','Ollenhauer Str. 96','Landhausstraße 31','Gruenauer Strasse 98','Lietzenburger Strasse 45','Kantstraße 14','Landhausstraße 97','Rankestraße 18','Friedrichstrasse 35','Kirchenallee 40','Neuer Jungfernstieg 32','Bissingzeile 39','Ellmenreichstrasse 98','Straße der Pariser Kommune 22','Flotowstr. 90','Eschenweg 8','Rhinstrasse 66','Luebeckertordamm 35','Boxhagener Str. 46','Hardenbergstraße 10','Prenzlauer Allee 57','Sonnenallee 71','Knesebeckstraße 18','Borstelmannsweg 18','Hollander Strasse 95','Los-Angeles-Platz 80','Oldesloer Strasse 7','Feldstrasse 30','Rathausstrasse 87','Billwerder Neuer Deich 98','Gubener Str. 40','Brandenburgische Str 79','Neue Roßstr. 82','Kantstraße 44','Luckenwalder Strasse 49','Pappelallee 29','Motzstr. 48','Eschenweg 29','Hoheluftchaussee 36','Borstelmannsweg 17','Lange Strasse 1','Gotzkowskystrasse 3','Rhinstrasse 91','Billwerder Neuer Deich 1','Lietzenburger Strasse 69','Augsburger Straße 32','Bleibtreustrasse 4','Knesebeckstrasse 64','Esplanade 80','Schmarjestrasse 99','An Der Urania 77','Rankestraße 21','Luebeckertordamm 57','Friedrichstrasse 6','Charlottenstrasse 81','Rohrdamm 9','Rathausstrasse 59','Jahnstrasse 40','Koenigstrasse 50','Frankfurter Allee 82','Rhinstrasse 35','Jahnstrasse 26','Straße der Pariser Kommune 83','Rhinstrasse 44','Buelowstrasse 28','Alt-Moabit 84','Fischerinsel 12','Fasanenstrasse 35','Mellingburgredder 21','Paul-Nevermann-Platz 8','Lützowplatz 39','Budapester Strasse 53','Hans-Grade-Allee 95','Sömmeringstr. 25','Kirchenallee 3','Scharnweberstrasse 26','Spresstrasse 96','Boxhagener Str. 73','Rohrdamm 62','Hochstrasse 53','Guentzelstrasse 70','Leipziger Straße 19','Ansbacher Strasse 13','Koepenicker Str. 73','Gotzkowskystraße 72','Pohlstrasse 1','Charlottenstrasse 88','Oldesloer Strasse 98','Hallesches Ufer 93','Sömmeringstr. 22','Kirchenallee 52','Prenzlauer Allee 3','Kantstraße 83','Adenauerallee 36','Luetzowplatz 86','An der Alster 18','Nuernbergerstrasse 99','Boxhagener Str. 80','Holstenwall 86','An der Schillingbrucke 43','Leipziger Straße 16','Paderborner Strasse 20','Hochstrasse 12','Budapester Strasse 14','Gotzkowskystraße 19','Brandenburgische Str 29','Hoheluftchaussee 35','Eichendorffstr. 27','Lietzenburger Straße 98','Hochstrasse 77','Luebeckertordamm 28','Prager Str 93','Neuer Jungfernstieg 99','Kantstraße 66','Brandenburgische Str. 72','Michaelkirchstr. 55','Friedrichstrasse 80','Hochstrasse 5','Mellingburgredder 99','Gruenauer Strasse 70','Holstenwall 18','Eschenweg 25','Scharnweberstrasse 50','Sömmeringstr. 49','Leipziger Strasse 1','Brandenburgische Str 76','Pasewalker Straße 69','Sonnenallee 66','Knesebeckstraße 65','Waldowstr. 36','Motzstr. 9','Straße der Pariser Kommune 83','Kastanienallee 34','Prager Str 41','Kantstrasse 55','Mellingburgredder 92','Park Str. 52','Hedemannstasse 44','Grolmanstraße 51','Charlottenstrasse 36','Konstanzer Strasse 69','Eschenweg 54','Joachimstaler Str. 86','Landhausstraße 73','Adenauerallee 29','Konstanzer Strasse 5','Gubener Str. 29','Langenhorner Chaussee 49','Fischerinsel 28','Rudower Strasse 75','Neue Roßstr. 18','Guentzelstrasse 41','Langenhorner Chaussee 85','Bissingzeile 99','Hermannstrasse 44','Fasanenstrasse 20','Chausseestr. 37','Wallstrasse 22','Alt-Moabit 68','An der Alster 79','Oldesloer Strasse 50','An der Schillingbrucke 91','Flotowstr. 13','Hoheluftchaussee 51','Meininger Strasse 86','Mühlenstrasse 23','Kirchenallee 85','Nuernbergerstrasse 71','Karl-Liebknecht-Strasse 82','Bayreuther Strasse 3','Stuttgarter Platz 71','Brandenburgische Str 81','Kurfuerstendamm 85','Park Str. 46','Prenzlauer Allee 39','Karl-Liebknecht-Strasse 44','Fontenay 27','Schoenebergerstrasse 36','Potsdamer Platz 80','Unter den Linden 93','Fontenay 49','Scharnweberstrasse 39','Knesebeckstrasse 6','Alsterkrugchaussee 9','Mollstrasse 16','Lange Strasse 89','Motzstr. 46','Wallstrasse 51','Ziegelstr. 95','Rathausstrasse 13','Waßmannsdorfer Chaussee 99','Ufnau Strasse 34','Augsburger Straße 10','Schillerstrasse 95','Mühlenstrasse 95','Charlottenstrasse 99','Michaelkirchstr. 42','Güntzelstrasse 58','Fischerinsel 45','Hoheluftchaussee 90','Schönhauser Allee 82','Heinrich Heine Platz 25','Pasewalker Straße 45','Pasewalker Straße 24','Rudower Strasse 35','Büsingstrasse 38','Hans-Grade-Allee 46','Kurfürstendamm 52','Ellmenreichstrasse 91','Jahnstrasse 33','Feldstrasse 43','Am Borsigturm 21','Hochstrasse 85','Reeperbahn 64','Augsburger Straße 64','Bayreuther Strasse 55','Amsinckstrasse 5','Kieler Strasse 55','Brandenburgische Str 40','Karl-Liebknecht-Strasse 19','Koepenicker Str. 62','Lietzensee-Ufer 62','Rhinstrasse 7','Kurfürstendamm 32','Marseiller Strasse 47','Fasanenstrasse 82','Sömmeringstr. 33','Unter den Linden 80','Waldowstr. 67','Hallesches Ufer 71','Los-Angeles-Platz 34','Kantstraße 3','Lietzensee-Ufer 38','Augsburger Straße 6','Am Borsigturm 51','Kantstrasse 63','Heinrich Heine Platz 46','Kastanienallee 47','Amsinckstrasse 57','Budapester Strasse 44','Bleibtreustraße 45','Flughafenstrasse 11','Jenaer Strasse 11','Ufnau Strasse 20','Leipziger Strasse 38','Sömmeringstr. 77','Gubener Str. 60','Lietzenburger Strasse 91','Neue Roßstr. 84','Budapester Straße 1','Meinekestraße 32','Fischerinsel 60','Buelowstrasse 82','Kirchenallee 56','Waldowstr. 95','Eschenweg 41','Meinekestraße 81','Hardenbergstraße 89','Genterstrasse 11','Meinekestraße 17','Messedamm 67','Rosenstrasse 39','Joachimstaler Str. 67','Feldstrasse 23','Güntzelstrasse 15','Ellmenreichstrasse 69','Heinrich Heine Platz 52','Schoenebergerstrasse 8','Ellmenreichstrasse 4','Gruenauer Strasse 36','Flughafenstrasse 71','Stuttgarter Platz 16','Lietzenburger Straße 75','Kastanienallee 54','Ellmenreichstrasse 19','Eschenweg 73','Prenzlauer Allee 74','Koepenicker Str. 75','Gruenauer Strasse 79','Schoenebergerstrasse 18','Albrechtstrasse 65','Alter Wall 63','Hoheluftchaussee 99','Luebecker Strasse 54','Brandenburgische Str. 85','Kantstraße 46','Grosse Praesidenten Str. 67','Landhausstraße 5','Heiligengeistbrücke 61','Ziegelstr. 74','Kantstraße 8','Rankestraße 2','Pappelallee 50','Gotzkowskystraße 82','Augsburger Strasse 71','Leipziger Strasse 37','Meinekestraße 31','Rudolstaedter Strasse 51','Los-Angeles-Platz 11','Prenzlauer Allee 88','Kurfuerstendamm 59','Alter Wall 30','Straße der Pariser Kommune 70','An der Schillingbrucke 85','Meininger Strasse 25','Leipziger Strasse 69','Rhinstrasse 95','Lietzensee-Ufer 99','Fugger Strasse 12','Kantstrasse 64','Kieler Strasse 20','Neuer Jungfernstieg 50','Mühlenstrasse 59','Rhinstrasse 38','Mühlenstrasse 24','Lietzenburger Strasse 61','Heinrich Heine Platz 67','Park Str. 39','Hedemannstasse 39','Messedamm 72','Karl-Liebknecht-Strasse 27','Hollander Strasse 5','Bleibtreustraße 42','Bissingzeile 55','Budapester Straße 98','Rosenstrasse 41','Hermannstrasse 43','Lietzenburger Strasse 85','Potsdamer Platz 7','Budapester Straße 6','Ziegelstr. 3','Borstelmannsweg 54','Brandenburgische Straße 57','Los-Angeles-Platz 45','Budapester Straße 67','Eschenweg 35','Waßmannsdorfer Chaussee 61','Reeperbahn 73','Kurfürstenstraße 28','Pappelallee 34','Esplanade 70','Gotzkowskystrasse 25','Büsingstrasse 77','Leipziger Strasse 72','Boxhagener Str. 15','Rudower Chaussee 56','Alsterkrugchaussee 20','Hedemannstasse 48','Leipziger Strasse 6','Koenigstrasse 4','Wallstrasse 1','Lützowplatz 51','Schoenebergerstrasse 98','Grosse Praesidenten Str. 95','Brandenburgische Strasse 27','Konstanzer Strasse 89','Gotzkowskystraße 85','Augsburger Strasse 95','Rhinstrasse 76','Gubener Str. 83','Leopoldstraße 4','Messedamm 85','Ollenhauer Str. 59','Boxhagener Str. 53','Knesebeckstrasse 5','Lietzensee-Ufer 13','Kieler Strasse 18','Feldstrasse 60','Meinekestraße 99','Alsterkrugchaussee 87','Prenzlauer Allee 99','Buelowstrasse 26','Park Str. 52','Leipziger Straße 74','Heiligengeistbrücke 68','Storkower Strasse 36','Messedamm 41','Luetzowplatz 13','Joachimstaler Str. 60','Rohrdamm 92','Lützowplatz 70','Mellingburgredder 18','Prenzlauer Allee 5','Brandenburgische Str 56','Alsterkrugchaussee 25','Leipziger Straße 11','An der Alster 93','Alt Reinickendorf 12','Güntzelstrasse 84','Marseiller Strasse 37','Grosse Praesidenten Str. 76','Pohlstrasse 8','An Der Urania 86','Luckenwalder Strasse 84','Knesebeckstraße 24','Pohlstrasse 39','Chausseestr. 41','Invalidenstrasse 3','Knesebeckstrasse 34','Luebecker Strasse 99','Mollstrasse 3','Fasanenstrasse 63','Rudower Strasse 24','Frankfurter Allee 67','Hans-Grade-Allee 86','Waßmannsdorfer Chaussee 27','Amsinckstrasse 90','Gotzkowskystraße 75','Lietzenburger Straße 88','Büsingstrasse 38','Heinrich Heine Platz 3','Amsinckstrasse 10','Brandenburgische Strasse 32','Pohlstrasse 42','Nuernbergerstrasse 71','Augsburger Straße 22','Bayreuther Strasse 22','Flughafenstrasse 18','Grosse Praesidenten Str. 82','Brandenburgische Strasse 41','Augsburger Straße 32','Fontenay 68','Hans-Grade-Allee 22','Ellmenreichstrasse 43','Invalidenstrasse 85','Hardenbergstraße 63','Amsinckstrasse 7','Rohrdamm 85','Koenigstrasse 54','Leopoldstraße 93','Hermannstrasse 54','Schillerstrasse 24','Schönhauser Allee 51','Invalidenstrasse 36','Eschenweg 8','Koenigstrasse 55','Fischerinsel 55','Neue Roßstr. 94','Marseiller Strasse 56','Gotzkowskystraße 84','Gotthardstrasse 44','Augsburger Strasse 77','Prenzlauer Allee 71','Leipziger Straße 27','Lange Strasse 93','Kirchenallee 77','Luebecker Strasse 37','Chausseestr. 11','Nuernbergerstrasse 58','Kantstrasse 44','Flotowstr. 44','Ruschestrasse 16','Hermannstrasse 30','Hoheluftchaussee 42','Alsterkrugchaussee 76','Messedamm 21','An Der Urania 50','Schönwalder Allee 86','Sömmeringstr. 65','Los-Angeles-Platz 65','Kirchenallee 82','Heiligengeistbrücke 60','Messedamm 15','Karl-Liebknecht-Strasse 43','Brandenburgische Str. 77','Waldowstr. 84','Gubener Str. 23','Charlottenstrasse 64','Amsinckstrasse 45','Eschenweg 10','Boxhagener Str. 85','Kantstrasse 23','Alt-Moabit 89','Meininger Strasse 33','Messedamm 5','Kantstraße 48','Nuernbergerstrasse 87','Meininger Strasse 13','Alter Wall 10','Mohrenstrasse 60','Schönwalder Allee 95','Neue Roßstr. 52','Luebecker Strasse 17','Prenzlauer Allee 87','Grolmanstraße 71','Neuer Jungfernstieg 63','Lietzenburger Straße 73','Holstenwall 55','Luebeckertordamm 14','An Der Urania 13','Holstenwall 75','An der Schillingbrucke 67','Landhausstraße 90','Genterstrasse 61','Ansbacher Strasse 84','Hallesches Ufer 30','Holstenwall 46','Brandenburgische Straße 67','An Der Urania 85','An der Schillingbrucke 40','Gotzkowskystraße 72','Ansbacher Strasse 38','Bleibtreustrasse 14','Augsburger Strasse 79','Bissingzeile 71','Gotthardstrasse 23','Flughafenstrasse 31','Hans-Grade-Allee 21','Rathausstrasse 13','Genterstrasse 1','Landsberger Allee 83','Potsdamer Platz 59','Flughafenstrasse 10','Brandenburgische Straße 71','Lange Strasse 63','Schmarjestrasse 75','Brandenburgische Straße 69','Rohrdamm 35','Grolmanstraße 83','Ansbacher Strasse 1','Budapester Strasse 25','Storkower Strasse 90','Bissingzeile 46','Hardenbergstraße 40','Bissingzeile 63','Schönhauser Allee 37','Fontenay 9','Waldowstr. 22','Luebeckertordamm 46','Rosenstrasse 47','Eichendorffstr. 92','Frankfurter Allee 41','Motzstr. 19','Leopoldstraße 45','Ollenhauer Str. 91','Luebeckertordamm 29','Charlottenstrasse 43','Alt-Moabit 61','Schönwalder Allee 46','Buelowstrasse 75','Inge Beisheim Platz 51','Budapester Straße 7','Bayreuther Strasse 24','Jenaer Strasse 76','Inge Beisheim Platz 72','Park Str. 63','Mohrenstrasse 65','Kantstrasse 86','Lange Strasse 33','Alsterkrugchaussee 56','Alt Reinickendorf 48','Hallesches Ufer 81','Ruschestrasse 89','Kurfürstendamm 82','Michaelkirchstr. 32','Budapester Strasse 64','Heinrich Heine Platz 89','Alt-Moabit 42','Pohlstrasse 91','Karl-Liebknecht-Strasse 32','Schönhauser Allee 55','Schmarjestrasse 99','Karl-Liebknecht-Strasse 32','Ansbacher Strasse 24','Prager Str 66','Stresemannstr. 81','Frankfurter Allee 14','Esplanade 1','Billwerder Neuer Deich 3','Luetzowplatz 56','Reeperbahn 88','Ziegelstr. 74','Leipziger Straße 24','Nuernbergerstrasse 2','Kurfuerstendamm 87','Hardenbergstraße 85','Reeperbahn 27','Hollander Strasse 53','Boxhagener Str. 78','Alsterkrugchaussee 91','Kurfuerstendamm 83','Mühlenstrasse 66','Jahnstrasse 67','Spresstrasse 48','Invalidenstrasse 64','Karl-Liebknecht-Strasse 75','Schaarsteinweg 1','Schönwalder Allee 24','Mohrenstrasse 51','Bleibtreustrasse 21','Gotthardstrasse 19','Hedemannstasse 10','Rhinstrasse 66','Gotzkowskystraße 35','Wallstrasse 64','Rankestraße 91','Hochstrasse 34','Kirchenallee 17','Genslerstraße 36','Luetzowplatz 63','Heiligengeistbrücke 60','Guentzelstrasse 87','Gotzkowskystrasse 59','Bissingzeile 29','Hochstrasse 90','Hochstrasse 75','Scharnweberstrasse 77','Schmarjestrasse 4','Oldesloer Strasse 98','Landhausstraße 65','Schoenebergerstrasse 2','Boxhagener Str. 72','Joachimstaler Str. 67','Unter den Linden 5','Pohlstrasse 62','Knesebeckstraße 95','Heiligengeistbrücke 69','Am Borsigturm 77','Rathausstrasse 42','Ziegelstr. 69','Ufnau Strasse 8','Hollander Strasse 87','Rudower Strasse 74','Invalidenstrasse 22','Genterstrasse 63','Büsingstrasse 78','Brandenburgische Straße 87','Rosenstrasse 50','Kastanienallee 39','Augsburger Straße 29','Genterstrasse 1','Lietzenburger Strasse 67','Lange Strasse 73','Ollenhauer Str. 62','Brandenburgische Strasse 84','Knesebeckstrasse 13','Brandenburgische Str. 43','Sömmeringstr. 86','Nuernbergerstrasse 71','Lietzenburger Straße 39','Rudower Strasse 33','Kurfuerstendamm 45','Alt Reinickendorf 20','Gotzkowskystrasse 31','Amsinckstrasse 94','Nuernbergerstrasse 80','Leipziger Strasse 65','Heiligengeistbrücke 48','Ziegelstr. 49','Leipziger Strasse 93','Waßmannsdorfer Chaussee 57','Albrechtstrasse 47','Wallstrasse 95','Leipziger Strasse 3','Genslerstraße 40','Billwerder Neuer Deich 42','Buelowstrasse 10','Flotowstr. 73','Sonnenallee 55','Hermannstrasse 35','Ruschestrasse 61','Borstelmannsweg 73','Buelowstrasse 40','Meinekestraße 73','Schoenebergerstrasse 11','Spresstrasse 29','Schönhauser Allee 55','Lützowplatz 13','Langenhorner Chaussee 86','Schillerstrasse 89','Koepenicker Str. 73','Leipziger Strasse 26','Leipziger Straße 88','Güntzelstrasse 1','Budapester Straße 64','Ansbacher Strasse 53','Heiligengeistbrücke 56','Jahnstrasse 73','Oldesloer Strasse 91','Ellmenreichstrasse 33','Jenaer Strasse 67','Inge Beisheim Platz 37','Gotzkowskystraße 7','Budapester Strasse 98','Pappelallee 24','Nuernbergerstrasse 77','Rankestraße 48','Jenaer Strasse 20','Eichendorffstr. 14','Fasanenstrasse 70','Ufnau Strasse 20','Meininger Strasse 53','Schönwalder Allee 55','Koepenicker Str. 28','Knesebeckstraße 45','Marseiller Strasse 16','Hoheluftchaussee 8','Bayreuther Strasse 86','Alt Reinickendorf 4','Anhalter Strasse 93','Feldstrasse 5','Lietzenburger Straße 73','Rankestraße 73','Knesebeckstraße 26','Kieler Strasse 58','Waldowstr. 70','Budapester Straße 95','Langenhorner Chaussee 41','Ziegelstr. 30','Neue Roßstr. 91','Flotowstr. 59','Kieler Strasse 59','Buelowstrasse 71','Frankfurter Allee 73','Genslerstraße 33','Landsberger Allee 74','Hallesches Ufer 26','Meinekestraße 66','Luebecker Strasse 1','Budapester Strasse 82','Alt Reinickendorf 88','Brandenburgische Straße 63','Kurfürstenstraße 1','Rhinstrasse 4','Motzstr. 82','Fontenay 8','Joachimstaler Str. 14','Rudower Strasse 23','Pappelallee 46','Prager Str 70','Paul-Nevermann-Platz 29','Karl-Liebknecht-Strasse 18','Schillerstrasse 29','Holstenwall 61','Gotthardstrasse 63','Gotzkowskystrasse 45','Hedemannstasse 81','Mühlenstrasse 64','Landsberger Allee 66','Unter den Linden 29','Fugger Strasse 73','Hans-Grade-Allee 78','Genslerstraße 10','Hans-Grade-Allee 15','Neuer Jungfernstieg 37','Bissingzeile 60','Rankestraße 45','Hollander Strasse 35','Lietzenburger Strasse 82','Stuttgarter Platz 48','Hans-Grade-Allee 66','Reeperbahn 37','Büsingstrasse 28','Meininger Strasse 46','Schaarsteinweg 39','Fontenay 70','Frankfurter Allee 4','Buelowstrasse 41','Waldowstr. 67','Luetzowplatz 2','Amsinckstrasse 98','Ruschestrasse 79','Leopoldstraße 60','Ufnau Strasse 40','Rhinstrasse 79','Unter den Linden 22','Güntzelstrasse 89','Hoheluftchaussee 73','Mühlenstrasse 3','Pasewalker Straße 94','Gotzkowskystrasse 50','Bleibtreustraße 82','Eschenweg 60','Gotzkowskystrasse 53','Koepenicker Str. 77','Joachimstaler Str. 64','Holstenwall 22','Knesebeckstraße 4','Karl-Liebknecht-Strasse 37','Langenhorner Chaussee 40','Eichendorffstr. 32','Landsberger Allee 76','Holstenwall 39','Mohrenstrasse 56','Hans-Grade-Allee 62','Augsburger Straße 91'
];
$cities = ['Ingolstadt','Außernzell','Saalfeld','Willingen','Bruckerhof','Lütjenwestedt','Wuppertal Elberfeld','Freital','Pfarrweisach','Holzkirchen','Rostock','Berlin Friedrichsfelde','Remagen','Siegsdorf','Leuna','Koblenz Immendorf','Balje','Marktredwitz','Mainleus','Ensheim','Baruth','Kappeln','Bad Kissingen','Berlin Schöneberg','Drensteinfurt','Niederstaufenbach','Schönau','Deckenpfronn','Stralsund','Schönheide','Kipfenberg','Dresden','Nußloch','Neubrandenburg','Bollenbach','Kleinmaischeid','Amorbach','München','Mertesdorf','Pressig','Wierschem','Stahnsdorf','Bockenau','Bergisch Gladbach Lückerath','Oelsnitz','Hamburg Eidelstedt','Rieden','Hohenhameln','Böbingen','Loose','Herdorf','Ernst','Schüller','Dachau','Hillesheim','Hamburg Billwerder','Merseburg','Semmenstedt','Mehlmeisel','Thomasburg','Duingen','Ermsleben','Aurich','Ottenbach','Ober-Mörlen','Pößneck','Langenaltheim','Bedesbach','Suhl','Bergisch Gladbach Herkenrath','Potsdam','Ganderkesee','Frohburg','Mülheim an der Ruhr','Bremervörde','Essing','Olbersdorf','Suddendorf','Reutlingen Reicheneck','Zella-Mehlis','Bremerhaven Überseehafen','Zusmarshausen','St Wendel','Ölbronn-Dürrn','Nüsttal','Karlsruhe Mühlburg','Dahme','Halblech','Dolgesheim','Nürnberg','Pegnitz','Oberstaufen','Merchweiler','Wendelstein','Landshut','Aglasterhausen','Breitenthal','Bergheim','Neuburg','Berlin Hermsdorf','Oberheimbach','Mariaposching','Weißenburg','Schwarzach','Sanitz','Meerane','Hamburg Neustadt','Abentheuer','Schwaig','Altlandsberg','Luisenthal','Herzhorn','Hamburg Bahrenfeld','Thannhausen','Steinfeld','Grafenhausen','Kamp-Lintfort','Oettingen','Wiesbaden','Everode','Damflos','Bischberg','Flensburg','Linden','Bremen Bürgerpark','Spiegelau','Bayreuth','Lünen','Wilhelmshaven','Minden Aminghausen','Wingst','Wahlstedt','Berlin Mitte','Mühlacker','Kiefersfelden','Birkenwerder','Evessen','Hemslingen','Hoym','Winhöring','Daaden','Memmingen','Eppertshausen','Hamburg Veddel','Seelow','Bad Buchau','Oberaudorf','Rudolstadt','Rheurdt','Berlin Spandau','Hettstedt','Wißmannsdorf','Berlin Grünau','Glatten','Essen','Westertimke','Mannebach','Ebersbach','Oberasbach','Annaberg-Buchholz','Berlin Falkenberg','Hochstadt','Horstmar','Beckeln','Neunkirchen','Habichtswald','Hamburg Poppenbüttel','Kiel','Arnbruck','Amberg','Bad Orb','Trier Trier-Süd','Burgstetten','Baden-Baden','Zwiesel','Zwickau','Büsingen','Tönisvorst','Ansbach','Perl','Großmehring','Velpke','Weiden','Drohndorf','Haibach','Brackenheim','Rödental','Hochheim am Main','Faßberg','Düsseldorf Friedrichstadt','Berlin Prenzlauer Berg','Crailsheim','Altersbach','Knüllwald','Bochum','Landau in der Pfalz','Hammelburg','Bremen Borgfeld','Großniedesheim','Bottrop Batenbrock-Nord','Kirchhundem','Hohenroda','Garding','Laatzen','Freisen','Neuendorf','Nüdlingen','Triberg','Hunderdorf','Eberswalde','Neufahrn','Lucka','Albaching','Isselbach','Edling','Dorum','Losheim','Würzburg','Woltersdorf','Murg','Bodenbach','Grünstadt','Johanniskirchen','Boizenburg','Eschfeld','Reimlingen','Stotternheim','Regensburg','Adorf','Stuttgart Mitte','Schuttertal','Alheim','Cham','Willich','Borgentreich','Niederweiler','Mannheim Wohlgelegen','Hösbach','Wertingen','Gelsenkirchen','Steinmauern','Schwerin','Werneck','Finkenbach-Gersweiler','Grebenau','Reutlingen Innenstadt','Steindorf','Dobel','Forstern','Ebstorf','Geiersthal','Neuenkirchen','Hamburg Ohlsdorf','Minden Dützen','Ranschbach','Hemmelzen','Karlsruhe Daxlanden','Fürth','Berlin Marienfelde','Lautert','Ebernhahn','Hamburg Harvestehude','Metzenhausen','Forstinning','Mühlhausen','Konstanz Insel Mainau','Glöwen','Bottrop Süd-West-Innenstadt','Norderstedt','Olching','Heinrichsthal','Barntrup','Blunk','Hamburg Langenhorn','Radeberg','Bad Bramstedt','Deinstedt','Onsdorf','Hattingen Hattingen','Hamburg Sasel','Neumarkt-Sankt Veit','Bruckberg','Dietenhofen','Dielmissen','Friedeburg','Hornberg','Greußenheim','Balge','Banzkow','Magdeburg','Hatzenbühl','Creußen','Dersum','Stadtsteinach','Bremen Sodenmatt','Averlak','Frittlingen','Schweinfurt','Thalmässing','Brandenburg','Niedenstein','Überherrn','Nübbel','Auerberg','Lübtheen','Rottenacker','Cornberg','Aschenhausen','Pfaffenhofen','Biebern','Schönberg','Halle','Leipzig','Lebach','Weinsheim','Hüttblek','Hamburg Sinstorf','Röthenbach','Limbach','Tiefenbronn','Großheirath','Freiburg Munzingen','Bad Oeynhausen Innenstadt','Rollshausen','Hemmoor','Bischofswerda','Elbtal','Kördorf','Bekdorf','Seebach','Leiferde','Schwanstetten','Oberweiler','Eningen','Kröning','Euskirchen','Masburg','Aschaffenburg','Kesten','Kirchseeon','Ohmden','Weiskirchen','Gundheim','Altbach','Dürbheim','Bestwig','Esslingen Zollberg','Höheinöd','Korweiler','Reifenberg','Deggenhausertal','Oberammergau','Massing','Kleinostheim','Lahnstein','Neuenbürg','Weisweil','Ludwigshafen am Rhein West','Sehlem','Esslingen Wäldenbronn','Fischbach','Monheim','Neustadt an der Weinstraße Speyerdorf','Barweiler','Crimmitschau','Landesbergen','Zotzenheim','Freising','Huglfing','Görisried','Waidhofen','Kohlberg','Inzell','Grainet','Gerach','Breitengüßbach','Wuppertal Cronenberg','Bahrenborstel','Braunschweig','Tirschenreuth','Schlangenbad','Aventoft','Reichenbach','Haßfurt','Saßnitz','Plankenfels','Büren','Berlin Neukölln','Hamburg Groß Borstel','Bielefeld Eckardtsheim','Niederstetten','Eislingen','Brotterode','Jever','Rimbach','Schwangau','Hagermarsch','Pollenfeld','Bereborn','Jahnsdorf','Kamenz','Wewelsfleth','Jütebog','Bonn','Rimpar','Blankenheim','Bernau','Deggendorf','Zeiskam','Fürstenwalde','Bremen Rablinghausen','Bollingstedt','Habach','Grenderich','Haßmersheim','Kitzingen','Alf','Mandel','Schömberg','Paderborn Schloß Neuhaus','Helgoland','Barbelroth','Reichertshofen','Bechhofen','Traben-Trarbach','Kornwestheim','Berlin Tegel','Karstädt','Riesa','Herrischried','Dormagen Straberg','Schönebeck','Ginsweiler','Ortenburg','Chemnitz','Schmiechen','Pforzheim Würm','Winringen','Bad Berka','Strasburg','Berlin Charlottenburg','Görlitz','Illertissen','Mühldorf','Werdum','Vilseck','Dienstweiler','Malente','Esens','Glonn','Berlin Schmargendorf','Fronreute','Berlin Tempelhof','Bautzen','Wolfen','Niederlangen','Parkstein','Dettelbach','Umkirch','Brieden','Neuried','Todendorf','Einbeck','Merching','Karlsruhe Innenstadt-Ost','Wörth am Rhein','Hofheim','Mittelstrimmig','Freiberg','Nürtingen','Elmshorn','Zahna','Blaubach','Markranstädt','Sefferweich','Lambsheim','Lüdenscheid Bierbaum','Frestedt','Geilenkirchen','Gernsheim','Niederfrohna','Lengefeld','Lensahn','Weimar','Bosau','Stuttgart Nord','Langenzenn','Dingolshausen','Altenbeuthen','Unterschleißheim','Brunnthal','Stadtlohn','Bärnau','Münnerstadt','Parkstetten','Hamburg Eißendorf','Berg','Isselburg','Riedenburg','Dötlingen','Saarbrücken Malstatt','Stuttgart Freiberg','Eisenhüttenstadt','Schwarzenbek','Meckesheim','Gersdorf','Düsseldorf Grafenberg','Mamming','Dingolfing','Armstorf','Postbauer-Heng','Dortmund','Aschersleben','Clausberg','Scheinfeld','Hamburg Jenfeld','Welchweiler','Markkleeberg','Rosengarten','Hemer','Floß','Rauschenberg','Dietzhausen','Tangermünde','Kirchenlamitz','Bad Tölz','Rehborn','Burkhardtsdorf','Sarstedt','Olbernhau','Nack','Ilmenau','Hainichen','Rodach','Westernohe','Rehau','Ruppichteroth','Höxter','Dernbach','Obertraubling','Chamerau','Ahrenstedt','Berching','Pyrbaum','Erfurt','Bröckel','Rechtmehring','Worbis','Karlsruhe Südweststadt','Moosach','Niederroßbach','Luckenwalde','Viersen Schirick','Schöndorf','Neumünster','Hollingstedt','Hagenbach','Oberpöring','Maintal','Bielefeld Altenhagen','Hamburg Bramfeld','Coppenbrügge','Ahneby','Pfaffen-Schwabenheim','Bad Sachsa','Dillingen','Haverlah','Cottbus','Büna','Wadern','Blaustein','Aholming','Hohenstein-Ernstthal','Prenzlau','Laufeld','Salzwedel','Hamburg Francop','Kloschwitz','Merklingen','Gieboldehausen','Schwabmünchen','Westerland','Trappenkamp','Sinn','Dörth','Kyritz','Konstanz Petershausen','Damnatz','Kampen','Lauterhofen','Weßling','Mauern','Gornsdorf','Langenbrettach','Lübbecke','Oberhausen Stadtmitte','Elmlohe','Drage','Teutschenthal','Apolda','Ladenburg','Linkenheim-Hochstetten','Minden Meißen','Ney','Ispringen','Augsburg','Marl','Bad Mergentheim','Waldalgesheim','Köln Rodenkirchen','Wiesmoor','Mammendorf','Erlangen','Düsseldorf Stadtmitte','Binzen','Bremen Findorff-Bürgerweide','Roßdorf','Lahr','Bad Freienwalde','Eggesin','Bessenbach','Großkarolinenfeld','Marktschellenberg','Remscheid Lennep','Herzfeld','Künzell','Kalbach','Freilassing','Ludwigshöhe','Hamburg Rahlstedt','Halstenbek','Bad Neuenahr-Ahrweiler','Langenhagen','Christes','Hohenstadt','Herzberg','Neunkirchen Innenstadt','Miltach','Garrel','Struvenhütten','Frankfurt','Sendenhorst','Solingen Solingen-Mitte','Vahlde','Rodewisch','Bodolz','Marktleuthen','Mannheim Seckenheim','Birkenbeul','Lauf','Mainz Hetzheim','Pullach','Bad Münster am Stein-Ebernburg','Kesseling','Aldersbach','Hamburg Barmbek-Nord','Kaufbeuren','Lüdenscheid Freisenberg','Icking','Suthfeld','Deuselbach','Viersen Dornbusch','Breitenbach','Blumberg','Gotha','Ötigheim','Kyllburgweiler','Moosinning','Mölln','Thurnau','Bruckmühl','Breitenhagen','Velbert Bonsfeld','Nieblum','Wiek','Witten Rüdinghausen','Reutlingen Lerchenbuckel','Königswartha','Meisenheim','Buchholz in der Nordheide','Kaub','Guben','Niederhagen','Kochel','Hamburg Niendorf','Pforzheim Weststadt','Bühlerzell','Neuhausen','Bernsdorf','Jessen','Herne','Bottrop Welheimer Mark','Wilthen','Schönborn','Weilheim','Neualbenreuth','Oberhausen Borbeck','Lachen','Wiesbaden-Mainz-Kostheim','Leimersheim','Bergisch Gladbach Hebborn','Trier Weismark-Feyen','Oberbillig','Niemetal','Viersen Dyck','Härtlingen','Niederwinkling','Creglingen','Lotte','Sankt Julian','Lengerich','Buch','Rübeland','Longuich','Goldkronach','Nordhausen','Glauchau','Kulmbach','Windischeschenbach','Assau','Himbergen','Aukrug','Schmalkalden','Eckersweiler','Raschau','Kempten','Ochsenfurt','Engelsdorf','Wachenroth','Oedheim','Ahorn','Schwedt','Gunzenhausen','Pfarrkirchen','Manching','Auerswalde','Kerken','Antweiler','Lembruch','Obernburg','Unterpleichfeld','Schemmerhofen','Schiffweiler','Bielefeld Windelsbleiche','Pirna','Polling','Marklkofen','Bergisch Gladbach Moitzfeld','Leverkusen','Kressbronn','Morsum','Balduinstein','Berghaupten','Wulsbüttel','Steffenberg','Biblis','Buchholz','Wasserburg','Niedererbach','Tittling','Coburg','Lichtenfels','Ahlbeck','Affalterbach','Lappersdorf','Büchel','Markt Schwaben','Ludwigshafen am Rhein Maudach','Hamburg Harburg','Schwörstadt','Buttstädt','Hammerstein','Nerdlen','Saal','Böllen','Rothenberg','Rees','Boxberg','Stuttgart Süd','Hamburg Eilbek','Schürdt','Weißwasser','Unterwössen','Havelberg','Georgsmarienhütte','Kaisheim','Spessart','Düsseldorf Wittlaer','Korbach','Bad Windsheim','Breitenau','Oberweis','Lindenschied','Edemissen','Geisfeld','Eckental','Lauschied','Schwabach','Walkenried','Bad Wilsnack','Thierhaupten','Thalfang','Heddesbach','Jerxheim','Büsum','Sehnde','Simmerath','Bergfeld','Borstel-Hohenraden','Kröppen','Idelberg','Schonungen','Mommenheim','Heilbach','Karlsruhe Innenstadt-West','Reinfeld','Hamburg Billstedt','Aupitz','Uedem','Eitting','Annaburg','Wernersberg','Großharrie','Waldbröl','Reutlingen Oststadt','Wattenheim','Castrop-Rauxel','Kulmain','Wanderup','Obertrubach','Hornbach','Hettenhausen','Lichtenberg','Hessisch Oldendorf','Steinbach','Heidelberg Kirchheim','Fischach','Rheinhausen','Neustadt an der Weinstraße Haardt','Remscheid Lüttringhausen','Fulda','Wessobrunn','Niederfischbach','Katlenburg-Lindau','Unterföhring','Schaufling','Beratzhausen','Malchin','Zetel','Bitterfeld','Ludwigslust','Dirmstein','Laucha','Kellinghusen','Neukirchen','Neckarbischofsheim','Blaibach','Heidmoor','Kehl','Sohland','Jena','Hamburg Kleiner Grasbrook','Lehnin','Auerbach','Bad Oldesloe','Hoppstädten','Arnstein','Trossingen','Rieseby','Trier Zewen','Scheßlitz','Bremen Lehe','Isterberg','Mücheln','Pommelsbrunn','Albersdorf','Klostermansfeld','Illerkirchberg','Teltow','Kinheim','Schmelz','Rosenthal','Brunsbüttel','Mietingen','Bad Sassendorf','Burgen','Gütersloh Spexard','Niederdreisbach','Wanna','Kemnath','Kronshagen','Egweil','Trabitz','Berlin Margarethenhöhe','Ottobrunn','Berlin Lübars','Bad Salzuflen Schötmar','Irmenach','Langwedel','Schenkelberg','Osten','Horschbach','Staufen','Weißenstadt','Langenburg','Hirschbach','Konstanz Industriegebiet','Karlsruhe Nordweststadt','Udenheim','Berlin Britz','Burgthann','Borgdorf-Seedorf','Hirten','Westergellersen','Herold','Reppenstedt','Düsseldorf Pempelfort','Meßstetten','Planegg','Wildpoldsried','Wismar','Oetzen','Breddorf','Heilbad Heiligenstadt','Langendorf','Calberlah','Helvesiek','Brigachtal','Fresdorf','Mastershausen','Eichenau','Karlsruhe Beiertheim-Bulach','Maxhütte-Haidhof','Ludwigsstadt','Hüttingen an der Kyll','Zinnowitz','Neuschönau','Holzminden','Glan-Münchweiler','Irfersgrün','Holzheim','Hamburg Farmsen-Berne','Büchenbach','Heilbronn Neckargartach','Bamberg','Reichertsheim','Ahlefeld','Rieschweiler-Mühlbach','Affinghausen','Schwarzenfeld','Rendsburg','Gutsbezirk Münsingen','Detmold Jerxen-Orbke','Kandern','Bocholt','Weißenthurm','Thyrnau','Hörnum','Bischofsmais','Kaschenbach','Rothenburg','Burghaun','Mündersbach','Anderlingen','Sylt-Ost','Wenningstedt','Mannheim Waldhof','Altglobsow','Alt Buchhorst','Mylau','Riesweiler','Grasleben','Monschau','Krefeld','Simbach','Marktoberdorf','Molsberg','Matzerath','Gommersheim','Schönkirchen','Poing','Berlin Mariendorf','Seefeld','Lüdenscheid Gevelndorf','Bremen Woltmershausen','Bergisch Gladbach Sand','Neubiberg','Stuttgart Stammheim','Oberhausen','Horhausen','Gechingen','Epfenbach','Bad Bentheim','Bleialf','Grünwald','Hindelang','Stein-Neukirch','Rückersdorf','Ludwigshafen am Rhein Oggersheim','Mitteleschenbach','Neureichenau','Hamburg Altona-Nord','Passau','Straßenhaus','Berlin Wannsee','Hattert','Wittnau','Zuzenhausen','Ballerstedt','Münk','Barnstorf','Plohn','Breisach','Großlittgen','Burghausen','Nastätten','Ludwigshafen am Rhein Edigheim','Breuna','Strullendorf','Birkigt','Lonnerstadt','Südlohn','Plön','Rohrenfels','Bärenthal','Aitrang','Eppelborn','Reichenberg','Alsdorf','Oberau','Böhlen','Reutlingen Gmindersdorf','Immenstadt','Mertesheim','Altenbrak','Baar-Ebenhausen','Langerwehe','Reit im Winkl','Kupferzell','Klingelbach','Ramsau','Saarbrücken Dudweiler','Altenburg','Burgkirchen','Hachenburg','Ravensburg','Berlin Steglitz','Hildrizhausen','Wetschen','Breitbrunn','Warder','Kettershausen','Berlin Marzahn','Rutesheim','Neuenmarkt','Gemmerich','Kesfeld','Augustusburg','Starnberg','Oberthulba','Sottrum','Scherstetten','Allstedt','Wollmerath','Neufra','Koblenz Oberwerth','Berlin Wilmersdorf','Kropp','Ebersdorf','Salzweg','Erlenbach bei Dahn','Bergisch Gladbach Asselborn','Daubach','Hüsby','Preetz','Haimhausen','Neunkirchen Hangard','Schmidgaden','Helmbrechts','Neu-Ulm','Tannhausen','Philippsheim','Prettin','Tangerhütte','Berlin Altglienicke','Berscheid','Bad Bibra','Ediger-Eller','Aach','Greimersburg','Hoppstädten-Weiersbach','Dessighofen','Amsdorf','Ahlen','Hoyerhagen','Konstanz Egg','Kemberg','Albstadt Ebingen','Stolk','Unna','Weinböhla','Osterbruch','Retzstadt','Kellenbach','Berlin Niederschönhausen','Hamburg Nienstedten','Eulgem','Hattingen Winz-Baak','Norderfriedrichskoog','Berlin Lichterfelde','Scheuring','Niebüll','Cadolzburg','Eschbach','Todtnau','Helbra','Schlüsselfeld','Warngau','Birkenheide','Klettgau','Geisenhausen','Engelstadt','Berlin Friedenau','Desloch','Freiburg Stadt','Lindhorst','Beedenbostel','Dietersburg','Wangen','Bremen Lessum','Unkel','Bestensee','Bad Endorf','Wulften','Busdorf','Almstedt','Osdorf','Netzschkau','Arenrath','Rech','Berlin Baumschulenweg','Elkenroth','Bernkastel-Kues','Alpen','Rüthen','Birkenstein','Kleinrinderfeld','Schnaitsee','Duisburg','Lamstedt','Sulzbach','Quickborn','Gröbenzell','Vohenstrauß','Nieheim','Langenargen','Rosenberg','Tegernsee','Ochtendung','Altenhagen','Holzweißig','Mühltal','Morbach','Rhede','Buchhorst','Raguhn','Altenriet','Elbenschwand','Blankensee','Witten Mitte','Winterburg','Bremen Neu Schwachhausen','Mannheim Feudenheim','Pantenburg','Altheim','Sankt Thomas','Hartmannsdorf','Nettersheim','Florstadt','Niederaula','Straubing','Wittgert','Hochspeyer','Züsch','Sellerich','Hergatz','Meinhard','Berlin Lankwitz','Urmitz','Geislingen','Jameln','Pluwig','Rathenow','Adelebsen','Viersen Lind','Berlin Rosenthal','Pentling','Griesheim','Hamburg Wilstorf','Velbert Mitte','Ortenberg','Gemünd','Leutershausen','Bremen Blumenthal','Guderhandviertel','Dassow','Großenwörden','Waldachtal','Itzgrund','Reichenau','Sonneberg','Eutin','Mannheim Sandhofen','Wipfeld','Hamburg Barmbek-Süd','Bredstedt','Stöckse','Neinstedt','Saarlouis','Hemsbünde','Schondorf','Balzhausen','Admannshagen','Eschenbach','Reichweiler','Böbrach','Kirchberg','Nohn','Oberhausen bei Kirn','Maihingen','Hütten','Barleben','Otterskirchen','Nusbaum','Friedrichsdorf','Genthin','Gaildorf','Göttingen','Berlin Kaulsdorf','Mömlingen','Grevenbroich Kapellen','Badra','Holzmaden','Teising','Lunden','Arnsberg','Winkelsett','Markertsgrün','Simmershofen','Miltenberg','Hutthurm','Gachenbach','Wolfenbüttel','Bad Zwischenahn','Hof','Herford Laar','Reipoltskirchen','Kirchheim','Schmölln','Meitingen','Herrnhut','Hürth','Neuruppin','Gräfelfing','Eisenach','Baesweiler','Goldbach','Friedrichroda','Birgel','Tennenbronn','Remshalden','Anklam','Oberhonnefeld-Gierend','Wesel','Lübeck','Geltendorf','Düsseldorf Hubbelrath','Spangenberg','Neuss Reuschenberg','Falkensee','Riedenberg','Schondra','Treuchtlingen','Kelsterbach','Trier Kürenz','Karlsruhe Stupferich','Bremerhaven Weddewarden','Adelsheim','Schauerberg','Lammershagen','Saarbrücken Bübingen','Unterwachingen','Nittel','Hirrlingen','Untermünkheim','Holste','Moosbach','Mönchengladbach Hardterbroich','Bad Oeynhausen Lohe','Neuenhaus','Ahrenshagen','Traunreut','Saarbrücken Brebach-Fechingen','Bergisch Gladbach Gronau','Glashütte','Großkarlbach','Sessenbach','Hitzhofen','Perscheid','Bergkirchen','Hahnheim','Oldenburg','Dahlem','Schacht-Audorf','Uetersen','Buchloe','Bad Liebenstein','Weißenfels','Horperath','Dietmannsried','Albig','Buggingen','Dornhan','Pilsach','Schenefeld','Seestermühe','Nürburg','Baldringen','Westhofen','Ringelai','Schöneberg','Langquaid','Minden Innenstadt','Dormitz','Altenkirchen','Bodenteich','Schleswig','Neubörger','Bruck','Boos','Ribbesbüttel','Wolfratshausen','Malching','Schrecksbach','Staffelstein','Hagenow','Lohsa','Niederaichbach','Elsfleth','Niederwambach','Detmold Bentrup-Loßbruch','Hamburg Neugraben-Fischbek','Feusdorf','Kranichfeld','Berthelsdorf','Anschau','Messerich','Preist','Bellheim','Hohenau','Freudenberg','Maasen','Berkatal','Oberlahr','Villingen-Schwenningen Herzogenweiler','Immenstaad','Velden','Velgast','Kaufungen','Miehlen','Kißlegg','Hohenlockstedt','Graach an der Mosel','Bad Wiessee','Köln Nippes','Neuweiler','Pellworm','Neunkirchen Furpach','Puchheim','Malberg','Vögelsen','Dackscheid','Meuselwitz','Hirschaid','Nördlingen','Tauberbischofsheim','Viereth-Trunstadt','Ettringen','Rüber','Bersenbrück','Kirburg','Magstadt','Halberstadt','Schkölen','Westerrönfeld','Wildenberg','Brodersby','Dietfurt','Lorup','Lauterach','Rentweinsdorf','Ratzeburg','Hinterweiler','Bad Schussenried','Grömitz','Crivitz','Buchet','Wirscheid','Münsterappel','Kierspe','Behringen','Bielefeld Innenstadt','Erharting','Gersthofen','Karlsruhe Weststadt','Pilsting','Karlsruhe Palmbach','Gau-Odernheim','Kelbra','Dießen','Holle','Tacherting','Hemdingen','Pillig','Hüttisheim','Plettenberg','Hamburg Moorburg','Krumbeck','Wallertheim','Hamburg Winterhude','Senden','Poppenhausen','Brunnen','Waldeck','Großheide','Düsseldorf Lörick','Benndorf','Frauenwald','Grimma','Bielefeld Sieker','Steinkirchen','Isenburg','Bad Berneck','Hattingen Niederbonsfeld','Ebensfeld','Dürrholz','Erdmannhausen','Euerbach','Goldenstedt','Eberstadt','Reinsdorf','Giershausen','Reutlingen Mittelstadt','Lüdersburg','Schwarzen','Kalefeld','Arzberg','Stephansposching','Kaiserslautern Innenstadt','Siegen','Karlsruhe Oststadt','Rüdesheim am Rhein','Bielefeld Schröttinghausen','Pöcking','Unterstadion','Wutha-Farnroda','Schipkau','Münster','Berlin Borsigwalde','Reinbek','Schwarzerden','Juist','Birkenfeld','Emmerich','Strausberg','Gräfenberg','Berg im Gau','Nufringen','Altdorf','Ludwigshafen am Rhein Mundenheim','Düsseldorf Flingern Süd','Sprakensehl','Werdohl','Rhens','Heidelberg Altstadt','Falkenstein','Rellingen','Horbach','Burgwindheim','Barkenholm','Obernfeld','Bernsbach','Neubulach','Hamburg Osdorf','Dedenbach','Lindenfels','Leingarten','Berenbach','Hersbruck','Hattingen Blankenstein','Hatzenport','Pinneberg','Oberhausen Eisenheim','Bergisch Gladbach Kaule','Wittenberge','Gerdau','Schafflund','Großweil','Hawangen','Heidelberg Weststadt','Weiterstadt','Templin','Plochingen','Neuss Furth-Nord','Pforzheim Innenstadt','Heideck','Burgkemnitz','Hollnseth','Kleinkahl','Elfershausen','Holtland','Metelen','Bremen Huckelriede','Elskop','Düsseldorf Lohausen','Soderstorf','Jelmstorf','Laubach','Moselkern','Apfeltrach','Rosenheim','Irrel','Lingerhahn','Unterhaching','Waldböckelheim','Neustrelitz','Bassum','Rhodt unter Rietburg','Schörghof','Dessau','Bellenberg','Stelle-Wittenwurth','Apensen','Telgte','Mittelhof','Höchberg','Oberhambach','Reutlingen Voller Brunnen','Nimshuscheid','Visbek','Roßleben','Neu-Anspach','Zweibrücken','Seßlach','Hamburg Alsterdorf','Lobenstein','Lohkirchen','Heimborn','Berlin Frohnau','Weiherhammer','Tutzing','Mehring','Stuttgart Mühlhausen','Eschwege','Bad Sülze','Bekmünde','Gauting','Blievenstorf','Aulosen','Schliengen','Emerkingen','Wildflecken','Forst','Zeulenroda','Vaterstetten','Niederstedem','Burgstädt','Kahla','Johanngeorgenstadt','Kratzenburg','Schönwald','Eußerthal','Angermünde','Haselau','Bremen Grohn','Hamburg Stellingen','Hamburg Hausbruch','Obertshausen','Detmold Schönemark','Helmstedt','Rickenbach','Grabenstätt','Hamburg Horn','Hahnweiler','Niehl','Greifswald','Haldensleben','Ehrenkirchen','Reichertshausen','Deutsch Evern','Spesenroth','Lauchhammer','Vöhringen','Möhnesee','Neuheilenbach','Garmisch-Partenkirchen','Haide','Neuler','Abtswind','Schalkenmehren','Schweich','Lindau','Riedstadt','Rehbach','Fuldabrück','Langlingen','Stollberg','Riedhausen','Berlin Tiergarten','Neuss Furth-Süd','Imsweiler','Ammersbek','Berlin Schmöckwitz','Ratingen Zentrum','Sigmaringendorf','Neumarkt','Warthausen','Forbach','Ahorntal','Schrobenhausen','Gingst','Klempau','Seeburg','Koblenz Stadtmitte','Saarbrücken Alt-Saarbrücken','Damscheid','Steinhöring','Niedermoschel','Paderborn Marienloh','Stendal','Nienstädt','Trier Trier-Nord','Mögglingen','Geratskirchen','Hermsdorf','Schwindegg','Stuttgart Zuffenhausen','Bad Schmiedeberg','Spiesen-Elversberg','Stuttgart Kaltental','Gries','Naumburg','Blomesche Wildnis','Beverstedt','Steinefrenz','Gehrde','Heimbach','Wernberg-Köblitz','Höchstadt','Schlüchtern','Wölpinghausen','Mühlau','Wertach','Egestorf','Forchtenberg','Hecken','Waiblingen Kernstadt-Nord','Forstmehren','Wilsecker','Berlin Wedding','Esslingen Kennenburg','Nideggen','Stuttgart Weilimdorf','Eichendorf','Reutlingen Weststadt','Amtzell','Flöthe','Wiesloch','Elsenfeld','Neuhaus','Stuttgart Bergheim','Inden','Süderbrarup','Eggolsheim','Witten Herbede','Welver','Hermeskeil','Drestedt','Altwigshagen','Altenhof','Ober-Flörsheim','Leisnig','Lengede','Felsberg','Elze','Weil im Schönbuch','Dohren','Flöha','Lahntal','Brehna','Zeitz','Arnstorf','Bubach','Kranzberg','Bad Lauchstädt','Wassenach','Hammer','Neuss Augustinusviertel','Baunatal','Karlsruhe Hagsfeld','Bundorf','Bonndorf','Bad Abbach','Bremen Strom','Arenshausen','Crispendorf','Ergolding','Jöhstadt','Bottrop Süd','Heringen','Timmendorfer Strand','Dorfen','Alsbach-Hähnlein','Müssen','Natendorf','Berlin Buckow','Neuhardenberg','Gützkow','Meckenbeuren','Ensdorf','Eichstegen','Schopfloch','Sassenburg','Breitenhain','Denkte','Gehrden','Dennheritz','Ebermannsdorf','Büdingen','Gerbrunn','Braunfels','Scharnebeck','Ellerau','Waldbrunn','Derental','Gernrode','Niederdorfelden','Siershahn','Königsbrunn','Börslingen','Teublitz','Amöneburg','Reutlingen Gönningen','Haag','Birnbach','Greimerath','Burglengenfeld','Homburg','Bad Kleinen','Erolzheim','Carlsberg','Frielendorf','Hildesheim','Solingen Gräfrath','Elbingerode','Mainz Mainz','Wahrenholz','Partenheim','Heerstedt','Emmering','Zierenberg','Uelzen','Oberviechtach','Bogen','Murnau','Fachbach','Handewitt','Kludenbach','Friesoythe','Detmold Vahlhausen','Rantum','Aurachtal','Weinsberg','Lindenberg','Baiersdorf','Neuengörs','Stuttgart Rotenberg','Marpingen','Durbach','Rennertshofen','Hünxe','Wittstock','Rohrbach','Rotwandhaus','Rheine','Oggelshausen','Aspach','Erdweg','Erkelenz','Kleve','Langwieden','Iphofen','Rastorf','Hohenberg','Oeversee','Hameln','Boden','Bruckmaier','Sinntal','Harburg','Herzberg am Harz','Aull','Mitterteich','Ammeldingen an der Our','Hengersberg','Hamburg Hamm-Nord','Großröhrsdorf','Berlin Malchow','Buckau','Altshausen','Ampfing','Fredersdorf','Burg Stargard','Syke','Dittelsheim-Heßloch','Amerang','Greppin','Pleiskirchen','Düsseldorf Derendorf','Köln Chorweiler','Viersen Heyerhöfe','Hohenwestedt','Kaperich','Daleiden','Oppenau','Bremen Mittelshuchting','Wawern','Hamburg Billbrook','Meerbusch Lank-Latum','Aichstetten','Weißenohe','Lützelbach','Bismark','Stuttgart Zazenhausen','Geschwenda','Baden-Baden Balg','Kirchtimke','Döbeln','Spatzenhausen','Appen','Marne','Schleiz','Nordhorn','Bermel','Angelroda','Limeshain','Kall','Steinwenden','Sonderhof','Senftenberg','Herbstein','Mönchengladbach Uedding','Osterhofen','Adenstedt','Borler','Forchheim','Versmold','Olsdorf','Gappenach','Langenbach','Sömmerda','Esslingen Sulzgries','Koblenz Stolzenfels','Merzig','Goslar','Söchtenau','Ockenheim','Gehrweiler','Heidenau','Bad Schwartau','Sontheim','Bad Birnbach','Müsch','Thanstein','Nohfelden','Klüsserath','Löwenstedt','Becherbach bei Kirn','Radevormwald','Wietmarschen','Wuppertal Barmen','Güntersleben','Hamburg Borgfelde','Bremen Schönebeck','Hamburg Hoheluft-Ost','Neuötting','Quedlinburg','Brande-Hörnerkirchen','Heidenheim','Ahrensburg','Lauben','Schlier','Pommersfelden','Bielefeld Brackwede','Neustadt an der Weinstraße Geinsheim','Elmenhorst','Barmstedt','Steimel','Cranzahl','Steinigtwolmsdorf','Detmold Niewald','Stuttgart Feuerbach','Helpsen','Appenweier','Esslingen Oberhof','Hohenbrunn','Geiselhöring','Mittenaar','Karl','Brauweiler','Stadtilm','Köthen','Kröpelin','Lauenburg','Düsseldorf Gerresheim','Reimerath','Brilon','Sand','Gödenstorf','Amalienfelde','Probstzella','Pforzheim Nordstadt','Bad Neustadt','Kürnbach','Maxdorf','Kranenburg','Albertshofen','Gosheim','Berlin Rudow','Willroth','Paderborn Elsen','Landsberg','Bröthen','Ginsheim-Gustavsburg','Emsbüren','Horb','Bad Feilnbach','Lüdenscheid Lüdenscheid','Nittenau','Rosche','Viersen Rahser','Mengen','Tetenhusen','Witten Bommern','Dreiskau-Muckern','Diekhusen-Fahrstedt','Wadgassen','Scheuern','Neuenweg','Plattling','Voltlage','Gelting','Bingen','Wildau','Gevenich','Dausenau','Quendorf','Röthlein','Bad Salzschlirf','Vreden','Waake','Bielefeld Brake','Brunstorf','Nandlstadt','Mettlach','Waldkirchen','Pforzheim Südweststadt','Stuttgart Stuttgart Flughafen','Bad Klosterlausnitz','Darmstadt','Osterburg','Zeitlofs','Königsmoos','Bopfingen','Varel','Gattendorf','Mehlbach','Stockelsdorf','Obergünzburg','Simmozheim','Wernigerode','Gaienhofen','Mössingen','Berlin Heinersdorf','Egloffstein','Schwarzhofen','Kerpen','Wackersdorf','Hellertshausen','Pölich','Haselund','Winningen','Engelschoff','Reichenbach-Steegen','Abtlöbnitz','Seekirch','Woltershausen','Hörgertshausen','Nanzdietschweiler','Leinsweiler','Breckerfeld','Böttingen','Eibau','Gondenbrett','Büchenbeuren','Nabburg','Großbreitenbach','Düsseldorf Heerdt','Bad Heilbrunn','Kottenborn','Bad Bertrich','Hartenfels','Düsseldorf Benrath','Fohren-Linden','Patersberg','Düsseldorf Golzheim','Stuttgart Sommerrain','Gondelsheim','Ramsen','Bissendorf','Dittelbrunn','Stadtlauringen','Rammelsbach','Konstanz Königsbau','Surwold','Eitorf','Meckenbach','Wolfertschwenden','Karwitz','Mainhardt','Hilkenbrook','Lorch','Grub','Jandelsbrunn','Borken','Glasau','Itterbeck','Neukamperfehn','Peiting','Ebsdorfergrund','Waging','Breitscheidt','Bösel','Contwig','Partenstein','Bergisch Gladbach Refrath','Unterneukirchen','Attenkirchen','Plessa','Bremen Blockland','Haigerloch','Hattenhofen','Stammham','Wittdün','Hasselfelde','Billigheim','Trierweiler','Bauler','Alfeld','Haßbergen','Garching','Wolnzach','Stuttgart Rohracker','Hamburg Eimsbüttel','Kadenbach','Lohnweiler','Niederbreitbach','Berkoth','Horgenzell','Bahro','Trebbin','Lünebach','Haste','Ormont','Lonsheim','Abensberg','Grasberg','Thum','Stützerbach','Schleich','Unterkirnach','Eberdingen','Rettenbach','Hartenstein','Stuttgart Hedelfingen','Gräfendorf','Waldershof','Fürstenberg','Hockenheim','Sauerthal','Dimbach','Altenpleen','Edermünde','Ober Kostenz','Worms Pfiffligheim','Graben','Schwarzheide','Fahrenzhausen','Hamburg Allermöhe','Großaitingen','Bitz','Wirges','Dannstadt-Schauernheim','Dudenhofen','Zschopau','Gengenbach','Wolfstein','Nachtsheim','Luckenbach','Stuttgart Luginsland','Fischbachau','Mannheim Rheinau','Horben','Altmannstein','Grenzach-Wyhlen','Sandersdorf','Geslau','Eching','Pollhagen','Goldberg','Berlin Zehlendorf','Bremen Arbergen','Bad Düben','Neundorf','Hamburg Tonndorf','Neuendettelsau','Viersen Bockert','Udler','Schechen','Glücksburg','Wehr','Weil','Alfhausen'];

//==========================================================================================================
// For Cars
//==========================================================================================================
$car_status = ['bestellt', 'ausgeliefert', 'verunfallt', 'stillgelegt'];
$equipment = ['CL3','C20','I8D','KA2','N7V','PDA','PQD','PU7','QJ1','QQ4','QV3','S1X','S50','WQS','WSP','YBK','YBS','YBT','YEV','Z30','1D2','2PF','2Z0','4A3','6XD','7P1','7X5','8T2','9AK','9VD','9ZX','EA5','N2R','PCG','PGP','PNU','PU8','UH2','YJH','1N7','2H7','3L5','3NU','4I3','4X4','6E3','6SJ','7Y1','9JH','9VS'];
$insert_columns_cars = [
    'id' => "'{{CAR_ID}}'",
    'name' => "'{{CAR_NAME}}'",
    'date_entered' => "now()",
    'date_modified' => "now()",
    'modified_user_id' => "'1'",
    'created_by' => "'1'",
    'description' => "NULL",
    'deleted' => "0",
    'aud_bnr' => "'{{AUD_BNR}}'",
    'aud_car_status_dd' => "'ausgeliefert'",
    'aud_car_status_meta_dd' => "'aktiv'",
    'aud_manufacture_dd' => "NULL",
    'aud_model_dd' => "NULL",
    'aud_special_model_dd' => "NULL",
    'aud_commission_number_newada' => "'{{AUD_COMMISSION_NUMBER_NEWADA}}'",
    'aud_commission_year' => "NULL",
    'aud_vin' => "'{{AUD_VIN}}'",
    'aud_bonus_received_flag' => "0",
    'aud_bonusrelevant_flag' => "0",
    'aud_bonusrelevant_from' => "NULL",
    'aud_bonusrelevant_icon' => "NULL",
    'aud_bonusrelevant_until' => "NULL",
    'aud_receipt_type' => "''",
    'aud_receipt_number' => "'{{AUD_RECEIPT_NUMBER}}'",
    'aud_cancellation_receipt_number' => "''",
    'aud_delivery_date' => "'{{AUD_DELIVERY_DATE}}'",
    'aud_delivery_last_contact_date' => "'{{AUD_DELIVERY_LAST_CONTACT_DATETIME}}'",
    'aud_reseller_assignment_date' => "NULL",
    'aud_rebook_flag' => "0",
    'aud_sold_to_reseller_flag' => "0",
    'aud_color_code' => "''",
    'aud_details' => "'{{AUD_DETAILS}}'", //Audi A6 Avant
    'aud_equipment' => "'{{AUD_EQUIPMENT}}'",
    'aud_extend_guarantee_flag' => "0",
    'aud_gw_plus_flag' => "0",
    'aud_kilometer' => "'{{AUD_KILOMETER}}'", //kilometers travelled
    'aud_land_iso' => "'DEU'",
    'aud_model_year' => "{{AUD_MODEL_YEAR}}", //2011
    'aud_next_hu_appointment' => "NULL",
    'aud_origin' => "''",
    'aud_passenger' => "''",
    'aud_price' => "0.000000",
    'currency_id' => "NULL",
    'base_rate' => "NULL",
    'aud_registration_date' => "now()",
    'aud_vk' => "'M'",
    'aud_date_update' => "now()",
    'aud_source' => "'SCRIPT'",
    'aud_endcustomer_resell_date' => "NULL",
    'aud_asg_buying_date' => "NULL",
    'aud_agis_purchase_date' => "NULL",
    'aud_asg_end_date' => "NULL",
    'aud_asg_start_date' => "NULL",
    'aud_asg_flag' => "0",
    'aud_manufacturer' => "'Audi'",
    'aud_model_line' => "'{{AUD_MODEL_LINE}}'", //A6
    'aud_model_derivative' => "'{{AUD_MODEL_DERIVATIVE}}'", //Avant
    'aud_model_derivative_gearbox' => "'{{AUD_MODEL_DERIVATIVE_GEARBOX}}'", //(C6) 2.7 TDI [ab 2009, 4F50XH]
    'aud_key_newada' => "'{{AUD_KEY}}'", //4F50XH
    'aud_key_dsmariav' => "'{{AUD_KEY}}'",
    'aud_key_keytools' => "'{{AUD_KEY}}'",
    'aud_key_kuba' => "'{{AUD_KEY}}'",
    'aud_key_eva' => "'{{AUD_KEY}}'",
    'aud_key_vwfsag' => "'{{AUD_KEY}}'",
    'aud_key_agis' => "'{{AUD_KEY}}'",
    'team_id' => "'1'",
    'team_set_id' => "NULL",
    'assigned_user_id' => "NULL",
    'aud_dealer_to_car_status_dd' => "'{{AUD_DEALER_TO_CAR_STATUS_DD}}'",
    'aud_financing_type' => "''",
    'aud_sales_tax_indicator' => "0",
    'aud_production_week' => "''",
    'aud_seller_number' => "''",
    'aud_last_garage_visit_date' => "NULL",
    'aud_license_plate' => "'{{AUD_LICENSE_PLATE}}'", //K-AI 1904
    'aud_dealer_id' => "'{{AUD_DEALER_ID}}'",
    'aud_order_entry_date' => "NULL",
    'aud_sell_documented_flag' => "0",
    'aud_bonus_guaranteed_flag' => "0",
    'aud_lieferstatus' => "''"
];
$sql_cars = "INSERT INTO `aud_cars` (".implode(', ', array_keys($insert_columns_cars)).") VALUES ";
$sql_template_cars = "(".implode(', ', array_values($insert_columns_cars)).")";
//-------------------------------------------------------------------------------------------
$contact_to_car_status = ['Halter' => '56e4e51b-0d8e-b41f-fb91-57a36cb78197','Fahrer' => '356f305f-9fc1-06b3-b8bd-57a36df1507f','Rechnungsempfanger' => '356g305f-9fc1-06b4-b8bd-57a36df1507f'];
$sql_c2c = "INSERT INTO `aud_contact_to_cars` (`id`,`name`,`date_entered`,`date_modified`,`modified_user_id`,`created_by`,`description`,`deleted`,`aud_contacts_id`,`aud_cars_id`,`aud_aktiv_inactive_dd`,`aud_source`,`aud_buying_date`,`aud_vin`,`aud_commission_number_newada`,`aud_registration_date`,`aud_delivery_last_contact_date`,`aud_bonusrelevant_flag`,`aud_next_hu_appointment`,`aud_car_status_dd`,`aud_unique_flag`,`aud_bonusrelevant_icon`,`aud_contact_to_car_rel_mgnt_id`,`primary_address_street`,`primary_address_city`,`primary_address_state`,`primary_address_postalcode`,`primary_address_country`,`aud_manufacturer`,`aud_model_line`,`aud_model_derivative`,`aud_model_derivative_gearbox`,`aud_key_newada`,`aud_key_dsmariav`,`aud_key_keytools`,`aud_key_kuba`,`aud_key_eva`,`aud_key_vwfsag`,`aud_key_agis`,`team_id`,`team_set_id`,`acl_team_set_id`,`assigned_user_id`,`aud_relation_status_dd_rel_mgmt`,`aud_dealer_id`,`aud_contact_in_europe_flag`,`aud_contact_firstname`,`aud_contact_lastname`,`aud_contact_title`,`aud_contact_telephone`,`aud_contact_email`,`aud_mgnt_name`,`aud_mgnt_customer_group_unique_txt`,`aud_mgnt_car_relation_unique_txt`,`aud_mgnt_relation_car_unique_txt`,`aud_mgnt_maintenance_type_txt`,`aud_mgnt_bonus_prohibited_flag`,`aud_service_order_type`) VALUES ";
$sql_c2c_template = "('{{C2C_ID}}','{{C2C_NAME}}',NOW(),NOW(),'1','1',NULL,0,'{{ID}}','{{CAR_ID}}','aktiv','Script',NULL,'{{AUD_VIN}}',NULL,'{{REGISTERATION_DATE}}',NULL,'{{BONUS_RELEVANT_FLAG}}',NULL,'{{CAR_STATUS_DD}}',1,NULL,'{{C2C_REL_MGMT_ID}}',NULL,NULL,NULL,NULL,NULL,'Audi','{{AUD_MODEL_LINE}}','{{AUD_MODEL_DERIVATIVE}}','{{AUD_MODEL_DERIVATIVE_GEARBOX}}','{{AUD_KEY}}',NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,'1','{{C2C_REL_MGMT_NAME}}','{{AUD_DEALER_ID}}',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL)";
//-------------------------------------------------------------------------------------------
$dealer_to_car_status = ['', 'Verkauft_Privat', 'Verkauft_Wiederverkäufer', 'Nicht_Verkauft'];
$dealer_relation_status = ['Verwalter', 'Verkauf'];
$sql_d2c = "INSERT INTO `aud_dealer_to_cars` (`id`,`name`,`date_entered`,`date_modified`,`modified_user_id`,`created_by`,`description`,`deleted`,`aud_cars_id`,`aud_dealer_id`,`aud_rebook_request_from_dealer_id`,`aud_rebook_request_to_dealer_id`,`aud_rebook_launch_at`,`aud_rebook_accepted_at`,`aud_active_flag`,`aud_vin`,`aud_registration_date`,`aud_delivery_last_contact_date`,`aud_bonusrelevant_flag`,`aud_next_hu_appointment`,`aud_car_status_dd`,`aud_sold_to_reseller_flag`,`aud_reseller_assignment_date`,`aud_bnr`,`aud_source`,`aud_rebooking_accepted_flag`,`aud_manufacturer`,`aud_model_line`,`aud_model_derivative`,`aud_model_derivative_gearbox`,`aud_key_newada`,`aud_key_dsmariav`,`aud_key_keytools`,`aud_key_kuba`,`aud_key_eva`,`aud_key_vwfsag`,`aud_key_agis`,`team_id`,`team_set_id`,`assigned_user_id`,`aud_dealer_relation_status_dd`) VALUES ";
$sql_d2c_template = "('{{D2C_ID}}','{{CAR_NAME}}',NOW(),NOW(),'1','1',NULL,0,'{{CAR_ID}}','{{AUD_DEALER_ID}}',NULL,NULL,NULL,NULL,1,'{{AUD_VIN}}','{{REGISTERATION_DATE}}',NULL,'{{BONUS_RELEVANT_FLAG}}','','{{CAR_STATUS_DD}}',0,NULL,'{{AUD_BNR}}','Script',0,'Audi','{{AUD_MODEL_LINE}}','{{AUD_MODEL_DERIVATIVE}}','{{AUD_MODEL_DERIVATIVE_GEARBOX}}','{{AUD_KEY}}',NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,'{{DEALER_RELATION_STATUS}}')";

//==========================================================================================================

$sql_values_array = [];
$sql_email_addr_values = [];
$sql_email_bean_rel_values = [];
$sql_cars_array = [];
$sql_c2c_array = [];
$sql_d2c_array = [];
for ($i=0; $i < $arg_count ; $i++) { 
    // +49104723048225
    $fname = array_rand($fnames, 1);
    $lname = array_rand($lnames, 1);
    $title = array_rand($titles, 1);
    if (empty($title)) {
        $title = 2;
    }
    $salutation = array_rand($salutations, 1);
    $street = array_rand($street_addresses, 1);
    $city = array_rand($cities, 1);
    $id = generateRandomString(8, '0123456789abcdef').'-'.generateRandomString(4, '0123456789abcdef')
        .'-'.generateRandomString(4, '0123456789abcdef').'-'.generateRandomString(4, '0123456789abcdef').'-'.generateRandomString(12, '0123456789abcdef');
    $email_address_id = generateRandomString(8, '0123456789abcdef').'-'.generateRandomString(4, '0123456789abcdef')
        .'-'.generateRandomString(4, '0123456789abcdef').'-'.generateRandomString(4, '0123456789abcdef').'-'.generateRandomString(12, '0123456789abcdef');
    $cust_no_newada = generateRandomString(6, '0123456789abcdefghijklmnopqrstuvwxyz');
    // echo "UUID: ".print_r($uuid_arr, 1)."\n";

    // echo "fname: ".print_r($fname, 1)."\n";
    // echo "lname: ".print_r($lname, 1)."\n";
    // echo "title: ".print_r($title, 1)."\n";
    //==============================================================================================================
    // For Contacts
    //==============================================================================================================
    $sql_values = str_replace('{{SALUTATION}}', $salutations[$salutation], $sql_template);
    $sql_values = str_replace('{{SALUTATION_TRANSLATED}}', $salutations_ger[$salutations[$salutation]], $sql_values);
    $sql_values = str_replace('{{ID}}', $id, $sql_values);
    $sql_values = str_replace('{{FIRST_NAME}}', $fnames[$fname], $sql_values);
    $sql_values = str_replace('{{LAST_NAME}}', $lnames[$lname], $sql_values);
    $sql_values = str_replace('{{PHONE_HOME}}', '+'.generateRandomString(14, '0123456789'), $sql_values);
    $sql_values = str_replace('{{PHONE_MOBILE}}', '+'.generateRandomString(14, '0123456789'), $sql_values);
    $sql_values = str_replace('{{PHONE_WORK}}', '+'.generateRandomString(14, '0123456789'), $sql_values);
    $sql_values = str_replace('{{PHONE_FAX}}', '+'.generateRandomString(14, '0123456789'), $sql_values);
    $sql_values = str_replace('{{PRIMARY_ADDRESS_STREET}}', $street_addresses[$street], $sql_values);
    $sql_values = str_replace('{{PRIMARY_ADDRESS_CITY}}', $cities[$city], $sql_values);
    $sql_values = str_replace('{{PRIMARY_ADDRESS_POSTALCODE}}', generateRandomString(5, '0123456789'), $sql_values);
    $sql_values = str_replace('{{AUD_DEALER_ID}}', $arg_dealer_id, $sql_values);
    $sql_values = str_replace('{{AUD_DEALER_BNR}}', $arg_bnr, $sql_values);
    $sql_values = str_replace('{{AUD_PO_BOX}}', generateRandomString(5, '0123456789'), $sql_values);
    $sql_values = str_replace('{{AUD_BIRTHDAY_DAY}}', rand(1,30), $sql_values);
    $sql_values = str_replace('{{AUD_BIRTH_YEAR}}', rand(1965,1990), $sql_values);
    $sql_values = str_replace('{{AUD_TITLE_DD}}', $titles[$title], $sql_values);
    $sql_values = str_replace('{{AUD_PHONE_FAX_PRIVATE}}', '+'.generateRandomString(14, '0123456789'), $sql_values);
    $sql_values = str_replace('{{AUD_NEWADA_CUSTOMER_NUMBER}}', $cust_no_newada, $sql_values);

    $sql_values_array[] = $sql_values;
    $sql_values = null;

    //-----------------------------------------------------------------------------------------------
    $sql_values = str_replace('{{EMAIL_ADDR_ID}}', $email_address_id, $sql_email_addr_template);
    $sql_values = str_replace('{{FIRST_NAME}}', $fnames[$fname], $sql_values);
    $sql_values = str_replace('{{LAST_NAME}}', $lnames[$lname], $sql_values);

    $sql_email_addr_values[] = $sql_values;
    $sql_values = null;

    //-----------------------------------------------------------------------------------------------
    $sql_values = str_replace('{{EMAIL_ADDR_ID}}', $email_address_id, $sql_email_bean_rel_template);
    $sql_values = str_replace('{{ID}}', $id, $sql_values);

    $sql_email_bean_rel_values[] = $sql_values;
    $sql_values = null;

    //==============================================================================================================
    // For Cars
    //==============================================================================================================
    global $car_maintenance_mgmt;
    $car_id = generateRandomString(8, '0123456789abcdef').'-'.generateRandomString(4, '0123456789abcdef')
        .'-'.generateRandomString(4, '0123456789abcdef').'-'.generateRandomString(4, '0123456789abcdef').'-'.generateRandomString(12, '0123456789abcdef');
    $aud_vin = 'WAUZZZ'.generateRandomString(11, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');
    $car_key_index = array_rand($car_codes, 1);
    $aud_key = $car_codes[$car_key_index];
    $aud_receipt_number = generateRandomString(7, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');
    $delivery_date = date('Y-m-d', time() - (rand(1, 250) * 24*60*60));
    $aud_kilometer = rand(100, 5000);
    $aud_model_year = date('Y') - rand(0, 2);
    $d2c_status_index = array_rand($dealer_to_car_status, 1);
    $d2c_status = $dealer_to_car_status[$d2c_status_index];
    $aud_license_plate = 'L'.generateRandomString(2, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ').'-'.generateRandomString(2, '0123456789').' '.generateRandomString(4, '0123456789');
    $manufacturer = $car_maintenance_mgmt[$aud_key][1];
    $model_line = $car_maintenance_mgmt[$aud_key][2];
    $model_line_derivative = $car_maintenance_mgmt[$aud_key][3];
    $model_gearbox = $car_maintenance_mgmt[$aud_key][4];
    $car_name = $car_maintenance_mgmt[$aud_key][0];
    $equipment_indices = array_rand($equipment, rand(1,15));
    $equipment_array = array_map(function ($value) use ($equipment) {
        return $equipment[$value];
    }, $equipment_indices);

    $sql_values = str_replace('{{CAR_ID}}', $car_id, $sql_template_cars);
    $sql_values = str_replace('{{CAR_NAME}}', $car_name, $sql_values);
    $sql_values = str_replace('{{AUD_BNR}}', $arg_bnr, $sql_values);
    $sql_values = str_replace('{{AUD_COMMISSION_NUMBER_NEWADA}}', $cust_no_newada, $sql_values);
    $sql_values = str_replace('{{AUD_VIN}}', $aud_vin, $sql_values);
    $sql_values = str_replace('{{AUD_RECEIPT_NUMBER}}', $aud_receipt_number, $sql_values);
    $sql_values = str_replace('{{AUD_DELIVERY_DATE}}', $delivery_date, $sql_values);
    $sql_values = str_replace('{{AUD_DELIVERY_LAST_CONTACT_DATETIME}}', $delivery_date, $sql_values);
    $sql_values = str_replace('{{AUD_DETAILS}}', $car_name, $sql_values);
    $sql_values = str_replace('{{AUD_EQUIPMENT}}', implode(' ', $equipment_array), $sql_values);
    $sql_values = str_replace('{{AUD_KILOMETER}}', $aud_kilometer, $sql_values);
    $sql_values = str_replace('{{AUD_MODEL_YEAR}}', $aud_model_year, $sql_values);
    $sql_values = str_replace('{{AUD_MODEL_LINE}}', $model_line, $sql_values);
    $sql_values = str_replace('{{AUD_MODEL_DERIVATIVE}}', $model_line_derivative, $sql_values);
    $sql_values = str_replace('{{AUD_MODEL_DERIVATIVE_GEARBOX}}', $model_gearbox, $sql_values);
    $sql_values = str_replace('{{AUD_KEY}}', $aud_key, $sql_values);
    $sql_values = str_replace('{{AUD_DEALER_TO_CAR_STATUS_DD}}', $d2c_status, $sql_values);
    $sql_values = str_replace('{{AUD_LICENSE_PLATE}}', $aud_license_plate, $sql_values);
    $sql_values = str_replace('{{AUD_DEALER_ID}}', $arg_dealer_id, $sql_values);
    $sql_cars_array[] = $sql_values;
    $sql_values = null;

    //-----------------------------------------------------------------------------------------------
    $c2c_id = generateRandomString(8, '0123456789abcdef').'-'.generateRandomString(4, '0123456789abcdef')
        .'-'.generateRandomString(4, '0123456789abcdef').'-'.generateRandomString(4, '0123456789abcdef').'-'.generateRandomString(12, '0123456789abcdef');
    $car_status_index = array_rand($car_status, 1);
    $car_status_dd = $car_status[$car_status_index];
    $rel_id_index = array_rand($contact_to_car_status, 1);
    $rel_id = $contact_to_car_status[$rel_id_index];

    $sql_values = str_replace('{{C2C_ID}}', $c2c_id, $sql_c2c_template);
    $sql_values = str_replace('{{C2C_NAME}}', $car_name, $sql_values);
    $sql_values = str_replace('{{ID}}', $id, $sql_values);
    $sql_values = str_replace('{{CAR_ID}}', $car_id, $sql_values);
    $sql_values = str_replace('{{AUD_VIN}}', $aud_vin, $sql_values);
    $sql_values = str_replace('{{REGISTERATION_DATE}}', $delivery_date, $sql_values);
    $sql_values = str_replace('{{BONUS_RELEVANT_FLAG}}', '0', $sql_values);
    $sql_values = str_replace('{{CAR_STATUS_DD}}', $car_status_dd, $sql_values);
    $sql_values = str_replace('{{C2C_REL_MGMT_ID}}', $rel_id, $sql_values);
    $sql_values = str_replace('{{AUD_MODEL_LINE}}', $model_line, $sql_values);
    $sql_values = str_replace('{{AUD_MODEL_DERIVATIVE}}', $model_line_derivative, $sql_values);
    $sql_values = str_replace('{{AUD_MODEL_DERIVATIVE_GEARBOX}}', $model_gearbox, $sql_values);
    $sql_values = str_replace('{{C2C_REL_MGMT_NAME}}', $rel_id_index, $sql_values);
    $sql_values = str_replace('{{AUD_DEALER_ID}}', $arg_dealer_id, $sql_values);
    $sql_values = str_replace('{{AUD_KEY}}', $aud_key, $sql_values);
    $sql_c2c_array[] = $sql_values;
    $sql_values = null;

    //-----------------------------------------------------------------------------------------------
    $d2c_id = generateRandomString(8, '0123456789abcdef').'-'.generateRandomString(4, '0123456789abcdef')
        .'-'.generateRandomString(4, '0123456789abcdef').'-'.generateRandomString(4, '0123456789abcdef').'-'.generateRandomString(12, '0123456789abcdef');
    $dealer_rel_status_index = array_rand($dealer_relation_status, 1);
    $d2c_relation = $dealer_relation_status[$dealer_rel_status_index];

    $sql_values = str_replace('{{D2C_ID}}', $d2c_id, $sql_d2c_template);
    $sql_values = str_replace('{{CAR_NAME}}', $car_name, $sql_values);
    $sql_values = str_replace('{{CAR_ID}}', $car_id, $sql_values);
    $sql_values = str_replace('{{AUD_DEALER_ID}}', $arg_dealer_id, $sql_values);
    $sql_values = str_replace('{{AUD_VIN}}', $aud_vin, $sql_values);
    $sql_values = str_replace('{{REGISTERATION_DATE}}', $delivery_date, $sql_values);
    $sql_values = str_replace('{{BONUS_RELEVANT_FLAG}}', '0', $sql_values);
    $sql_values = str_replace('{{CAR_STATUS_DD}}', $car_status_dd, $sql_values);
    $sql_values = str_replace('{{AUD_BNR}}', $arg_bnr, $sql_values);
    $sql_values = str_replace('{{AUD_MODEL_LINE}}', $model_line, $sql_values);
    $sql_values = str_replace('{{AUD_MODEL_DERIVATIVE}}', $model_line_derivative, $sql_values);
    $sql_values = str_replace('{{AUD_MODEL_DERIVATIVE_GEARBOX}}', $model_gearbox, $sql_values);
    $sql_values = str_replace('{{AUD_KEY}}', $aud_key, $sql_values);
    $sql_values = str_replace('{{DEALER_RELATION_STATUS}}', $d2c_relation, $sql_values);
    $sql_d2c_array[] = $sql_values;
    $sql_values = null;

}

$sql .= ' '.implode(', ', $sql_values_array);
$sql_email_addr .= ' '.implode(', ', $sql_email_addr_values);
$sql_email_bean_rel .= ' '.implode(', ', $sql_email_bean_rel_values);
$sql_cars .= ' '.implode(', ', $sql_cars_array);
$sql_c2c .= ' '.implode(', ', $sql_c2c_array);
$sql_d2c .= ' '.implode(', ', $sql_d2c_array);

// echo "Going to execute queries:\nInserting contacts\n";
// $GLOBALS['db']->query($sql);
// echo "Inserting Email addresses\n";
// $GLOBALS['db']->query($sql_email_addr);
// echo "Inserting Email and Bean relation\n";
// $GLOBALS['db']->query($sql_email_bean_rel);

echo "-- Generated SQL\n-- --------------------\n-- --------------------\n".$sql.";\n\n";
echo "-- Email Address SQL\n-- -----------------------\n-- -----------------------\n".$sql_email_addr.";\n\n";
echo "-- Email Bean Relation SQL\n-- ---------------------------\n-- ---------------------------\n".$sql_email_bean_rel.";\n\n";
echo "-- ---------------------------------------------------------------------------------------------\n";
echo "-- For Cars\n";
echo "-- ---------------------------------------------------------------------------------------------\n";
echo "-- Cars\n-- --------------------\n-- --------------------\n".$sql_cars.";\n\n";
echo "-- Contact to Cars relations\n-- --------------------\n-- --------------------\n".$sql_c2c.";\n\n";
echo "-- Dealer to Car relations\n-- --------------------\n-- --------------------\n".$sql_d2c.";\n\n";

function generateRandomString($length = 10, $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') {
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
