#!/bin/bash
#
while [[ $# > 1 ]]
do
key="$1"

case $key in
    -b|--branch)
    BRANCH="$2"
    shift # past argument
    ;;
    --repo-dir)
    REPO_DIR="$2"
    shift # past argument
    ;;
    --remote)
    REMOTE="$2"
    shift # past argument
    ;;
    --default)
    BRANCH='audi_7.7.0.0_sprint1'
    REPO_DIR='Mango'
    ;;
    *)
            # unknown option
    ;;
esac
shift # past argument or value
done

if [[ $BRANCH = "" ]]
    then
        BRANCH="r10"
fi

if [[ $REPO_DIR = "" ]]
    then
        REPO_DIR="/var/www/html/Mango"
fi

if [[ $REMOTE = "" ]]
    then
        REMOTE="upstream"
fi

echo -e "\e[1mCurrent Directory: ${PWD}\e[0m"
echo -e "\e[32mGoing to perform a complete reset in Git Repo ${REPO_DIR} on Branch ${BRANCH}\e[0m"

cd $REPO_DIR
git fetch $REMOTE
git checkout $BRANCH
git rebase $REMOTE/$BRANCH
git reset --hard $REMOTE/$BRANCH