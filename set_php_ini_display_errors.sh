#!/bin/bash

while [[ $# > 1 ]]
do
key="$1"
case $key in
    --set)
    SET="$2"
    shift # past argument
    ;;
    *)
    # unknown option
    ;;
esac
shift # past argument or value
done

echo -e "\e[32mSetting display errors to On/Off in PHP.ini\e[0m"
cd /etc

# display_errors = Off
if [[ $SET == 'Off' ]]
    then
        sudo cp -rf php.ini.error_off php.ini
    else
        sudo cp -rf php.ini.error_on php.ini
fi

sudo service httpd restart

echo -e "\e[32mDone setting display_errors to $SET in php.ini\e[0m"