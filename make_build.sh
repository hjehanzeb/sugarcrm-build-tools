#!/bin/bash

while [[ $# > 1 ]]
do
key="$1"

case $key in
    -n|--name)
    NAME="$2"
    shift # past argument
    ;;
    --mango-dir)
    MANGO_DIR="$2"
    shift # past argument
    ;;
    -b|--branch)
    BRANCH="$2"
    shift # past argument
    ;;
    --remove-build-folder)
    REMOVE_BUILD_FOLDER="$2"
    shift # past argument
    ;;
    --target-dir)
    TARGET_DIR="$2"
    shift # past argument
    ;;
    --remote)
    REMOTE="$2"
    shift # past argument
    ;;
    --zip)
    ZIPPED="$2"
    shift # past argument
    ;;
    --silent-install)
    SILENTINSTALL="$2"
    shift # past argument
    ;;
    --skip-update)
    SKIPUPDATE="$2"
    shift # past argument
    ;;
    --skip-build)
    SKIPBUILD="$2"
    shift # past argument
    ;;

    --default)
    DEFAULT=build
    ;;
    *)
            # unknown option
    ;;
esac
shift # past argument or value
done

if [[ $MANGO_DIR = "" ]]
    then
        MANGO_DIR="/var/www/html/Mango"
fi

if [[ $TARGET_DIR = "" ]]
    then
        TARGET_DIR="/var/www/html/Sugar"
fi

if [[ $REMOTE = "" ]]
    then
        REMOTE="upstream"
fi

if [[ $ZIPPED = "" ]]
    then
        ZIPPED="no"
fi

if [[ $SKIPBUILD = "" ]]
    then
        if [[ $SKIPUPDATE = "" ]]
            then
                echo -e "\e[32mResetting Mango to branch '${BRANCH}'\e[0m"
                ./git_reset.sh -b $BRANCH --repo-dir $MANGO_DIR --remote $REMOTE
        fi

        echo -e "\e[32mMaking a new Build with name '${NAME}'\e[0m"

        cd $MANGO_DIR/build/rome
        php build.php --ver=7.7.2.0 --flav=ent --dir=sugarcrm --build_dir=$TARGET_DIR/$NAME

        # Changing the version of PHP required for sugar
        # cd /var/www/html
        # ./phpversionchange --build-path $TARGET_DIR/$NAME

        # Copying vendor files from Mango to our new build
        echo -e "\e[32mCopying vendor files from Mango to our new build\e[0m"
        cd $MANGO_DIR/sugarcrm/vendor
        cp -rf sebastian/. $TARGET_DIR/$NAME/ent/sugarcrm/vendor/sebastian
        cp -rf phpunit/. $TARGET_DIR/$NAME/ent/sugarcrm/vendor/phpunit

        echo -e "\e[32mMaking a local GIT repo\e[0m"
        # Making the build a local git repo
        cp $TARGET_DIR/.gitignore $TARGET_DIR/$NAME/.gitignore
        cd $TARGET_DIR/$NAME
        git init
        git config core.fileMode false
        git add .
        git commit -m "1st commit"
fi


# Zipping up the build if option is given
if [[ $ZIPPED = "yes" && $SILENTINSTALL = "" ]]
    then
        echo -e "\e[32mMaking Zip\e[0m"
        cd $TARGET_DIR
        zip -r $NAME.zip $NAME

        # If --remove-build-folder is given as an option, then remove build folder after zipping
        if [[ $REMOVE_BUILD_FOLDER = "yes" ]]
            then
                rm -rf $NAME/
        fi
fi


# If we need to install Sugar as well, silently
if [[ $SILENTINSTALL = "yes" || $SILENTINSTALL = "silent" ]]
    then
        echo -e "\e[32mInstalling Sugar...\e[0m"
        curl -XDELETE http://localhost:9200/*
        # Silent Installing
        # php /var/www/html/siAudi.php --build-name $NAME
        # cd $TARGET_DIR/$NAME/ent/sugarcrm
        # php cli/dataloader/populate.php
        
        # Installing (fully) using Selenium test case
        sudo /etc/init.d/xvfb start
        export DISPLAY=:10
        cd /var/www/html
        ./sugar_test_case --build /Sugar/$NAME
        java -jar selenese-runner.jar Sugar_Install
        # Calling 2nd time as the Test case fails the first time for no reason, maybe bug in selenese-runner.jar
        java -jar selenese-runner.jar Sugar_Install
        sudo /etc/init.d/xvfb stop

        # Committing again
        cd $TARGET_DIR/$NAME
        git add .; git commit -m "after install"
fi

echo -e "\e[32mBuild complete\e[0m"
