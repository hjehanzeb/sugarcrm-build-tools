#!/bin/bash

while [[ $# > 1 ]]
do
key="$1"

case $key in
    -b|--build)
    BUILD="$2"
    shift # past argument
    ;;
    -f|--file)
    FILE_PATH="$2"
    shift # past argument
    ;;
    -v|--verbose)
    VERBOSE="$2"
    shift # past argument
    ;;
    --default)
    DEFAULT=build
    ;;
    *)
            # unknown option
    ;;
esac
shift # past argument or value
done

echo $PWD
echo -e "\e[32mRun Tests on ${BUILD}\e[0m"

# Making the link proper for phpunit in vendor/bin
cd /var/www/html/Sugar/$BUILD/ent/sugarcrm/vendor/bin
rm -f phpunit
ln -s ../phpunit/phpunit/phpunit phpunit

# Assigning permissions to phpunit in vendor/bin
cd /var/www/html/Sugar/$BUILD/ent/sugarcrm/tests
chmod +xr ../vendor/bin/phpunit

# Check if --file argument is given, so that only single file test cases are executed
if [[ $FILE_PATH = "" ]]
    then
        ../vendor/bin/phpunit --testsuite "Sugar Customization Test Suite" --debug
    else
        ../vendor/bin/phpunit --verbose ../custom/tests/$FILE_PATH
fi

echo -e "\e[32mTests Run Completed\e[0m"
