<?php

$demoData = false;
$build_name = $argv[2];

$siteUrl =  "http://192.168.149.150/Sugar/$build_name/ent/sugarcrm";
$sugar_config_si = array (
	'default_permissions' =>
	  array (
	    'dir_mode' => 1533,
	    'file_mode' => 509,
	    'user' => '',
	    'group' => '',
	  ),
	'default_currency_iso4217' => 'USD',
	'default_currency_name' => 'US Dollars',
	'default_currency_significant_digits' => '2',
	'default_currency_symbol' => '$',
	'default_date_format' => 'Y-m-d',
	'default_decimal_seperator' => '.',
	'default_export_charset' => 'ISO-8859-1',
	'default_language' => 'en_us',
	'default_locale_name_format' => 's f l',
	'default_number_grouping_seperator'    => ',',
	'default_time_format' => 'H:i',
	'export_delimiter' => ',',
	'setup_db_admin_password' => 'sugarcrm',
	'setup_db_admin_user_name' => 'root',
	'setup_db_create_database' => '1',
	'setup_db_drop_tables' => '1',
	'setup_db_sugarsales_password' => 'sugarcrm',
	'setup_db_sugarsales_password_retype' => 'sugarcrm',
	'setup_db_sugarsales_user' => 'root',
	// TODO: IMPORTANT: this needs to be mapped to a correct value for config si: was $this->db->getSilentConfigType() in SIM
	'setup_db_type' => 'mysql',
	'setup_db_host_name' => 'localhost',
	'setup_db_port_num' => '3306',
	'setup_db_create_sugarsales_user' => 0,
	'setup_db_database_name' => "sugarcrm_$build_name",
	'setup_db_pop_demo_data' => 0,
	'dbUSRData' => 'same',
	'demoData' => 'no',
	'setup_license_key' => '1a0848b4dba81062d28bdbb1e00fc6f4',
	'setup_site_admin_user_name'=> 'admin',
	'setup_site_admin_password' => '123',
	'setup_site_url' => "{$siteUrl}",
	'setup_system_name' => 'SugarCRM '.date('Y-m-d H:i'),
	'setup_fts_type' => 'Elastic',
	'setup_fts_host' => 'localhost',
	'setup_fts_port' => '9200'
);

// Define the file path
$file = "/var/www/html/Sugar/$build_name/ent/sugarcrm/config_si.php";

// TODO: Refactor this to use the SilentInstallConfig object
// Prepare the config_si.php
$sugar_config_si = "<?php \n\$sugar_config_si = ".var_export($sugar_config_si, true)."; \n";
$handle = fopen($file,'w');
fwrite($handle,$sugar_config_si);
fclose($handle);

$siURL = "http://localhost/Sugar/$build_name/ent/sugarcrm/install.php?goto=SilentInstall&cli=true";

echo "Installing Silently the build\n";
// TODO: Update curl library?
$ch = curl_init($siURL);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 100);
curl_setopt($ch, CURLOPT_TIMEOUT, 0);
// curl_setopt($ch, CURLOPT_VERBOSE, true);
$result = curl_exec($ch);

if (strpos($result, 'Success!') === false) {
	$failureMessage = "Failure in SugarCRM silent install";
sprintf("%s: curl (%s): %s\n", $failureMessage, curl_errno($ch), curl_error($ch));
}
echo "Build installed\n";
